<?php


namespace App\Notes\Requests;

use App\Notes\Model\Note;
use Illuminate\Support\Facades\Gate;
use Infrastructure\Http\ApiRequest;

abstract class NoteRequestBase extends ApiRequest
{
    public function authorize()
    {
        $id = $this->route('id');

        if($id)
        {
            return Gate::allows($this->ability, Note::where('id', $id)->firstOrFail());
        }
        else
            return true;
    }
}
