<?php


namespace App\Notes\Requests;


class UpdateNoteRequest extends NoteRequestBase
{
    public function rules()
    {
        return [
            'title' => 'sometimes',
            'text' => 'sometimes'
        ];
    }
}
