<?php


namespace App\Notes\Requests;


use Infrastructure\Http\ApiRequest;

class CreateNoteRequest extends ApiRequest
{
    protected $isCreateRequest = true;

    public function rules()
    {
        return [
            'title' => 'required',
            'text' => 'sometimes'
        ];
    }
}
