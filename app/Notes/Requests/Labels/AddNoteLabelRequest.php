<?php


namespace App\Notes\Requests\Labels;


use App\Notes\Requests\NoteRequestBase;
use Infrastructure\Http\ApiRequest;

class AddNoteLabelRequest extends NoteRequestBase
{
    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}
