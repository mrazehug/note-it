<?php


namespace App\Notes;


use App\Notes\Model\Note;
use App\Users\Models\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class NoteServiceProvider extends ServiceProvider
{
    // These are the abilities that need specific
    // note and relation of the user with this note.
    private $ownerAbilities = [ 'delete-note', 'get-note', 'update-note', 'add-note-label', 'delete-note-label', 'get-note-labels' ];

    public function boot()
    {
        $this->mapRoutes();
        $this->mapPermissions();
    }

    private function mapRoutes()
    {
        Route::group([
            'namespace' => 'App\Notes\Controllers',
            'prefix' => '/api/v1'
        ], base_path('app/Notes/routes_v1.php'));
    }

    private function mapPermissions()
    {
        foreach ($this->ownerAbilities as $ability)
        {
            Gate::define($ability, function (User $user, Note $note){
                return $note->isOwner($user);
            });
        }
    }
}
