<?php


namespace App\Notes\Transformers;


use App\Labels\Model\Label;
use App\Notes\Model\Note;
use Infrastructure\Transformers\Transformer;
use League\Fractal\TransformerAbstract;

class NoteTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'labels'
    ];

    public function transform($note)
    {
        return $note->toArray();
    }

    public function includeLabels(Note $note)
    {
        return $this->collection($note->labels, function(Label $label){
            return $label->toArray();
        });
    }
}
