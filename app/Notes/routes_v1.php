<?php

Route::get('/notes', 'NoteController@index')
    ->middleware('auth:api')
    ->name('notes.index');
Route::post('/notes', 'NoteController@create')
    ->middleware('auth:api')
    ->name('notes.create');
Route::get('/notes/{id}', 'NoteController@read')
    ->middleware('auth:api')
    ->name('notes.read');
Route::patch('/notes/{id}', 'NoteController@update')
    ->middleware('auth:api')
    ->name('notes.update');
Route::delete('/notes/{id}', 'NoteController@delete')
    ->middleware('auth:api')
    ->name('notes.delete');

Route::get('/notes/{id}/labels', 'NoteLabelController@index')
    ->middleware('auth:api')
    ->name('notes.labels.index');
Route::post('/notes/{id}/labels', 'NoteLabelController@create')
    ->middleware('auth:api')
    ->name('notes.labels.create');
Route::delete('/notes/{id}/labels/{labelId}', 'NoteLabelController@delete')
    ->middleware('auth:api')
    ->name('notes.labels.delete');
