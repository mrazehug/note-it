<?php


namespace App\Notes\Repositories;


use App\Notes\Model\Note;
use Infrastructure\Database\Eloquent\Repository;

class NoteRepository extends Repository
{
    public function getModel()
    {
        return new Note();
    }

    public function create(array $data)
    {
        $note = $this->getModel();
        $note->fill($data);

        $note->save();

        return $note;
    }

    public function update(Note $note, array $data)
    {
        $note->fill($data);
        $note->save();
        return $note;
    }

    public function delete(Note $note)
    {
        $note->users()->detach();
        $note->delete();
    }

    public function getWhere(...$where)
    {
        return Note::where(...$where);
    }
}
