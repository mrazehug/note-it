<?php


namespace App\Notes\Model;


use App\Labels\Model\Label;
use App\Users\Models\User;
use Illuminate\Support\Facades\DB;
use Infrastructure\Database\Eloquent\ModelBase;

class Note extends ModelBase
{
    public $fillable = [ 'title', 'text' ];
    public $timestamps = true;
    public $hidden = [ 'pivot' ];

    public function labels()
    {
        return $this->belongsToMany(Label::class, 'notes_labels');
    }

    public function isOwner(User $user)
    {
        return DB::table('users_notes')->where([['note_id', $this->id], ['user_id', $user->id]])->first() != null;
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'users_notes');
    }

    public function canManage(User $user)
    {
        return $this->users()->where('id', $user->id)->first() != null;
    }
}
