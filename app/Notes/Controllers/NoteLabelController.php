<?php


namespace App\Notes\Controllers;


use App\Notes\Requests\Labels\AddNoteLabelRequest;
use App\Notes\Requests\Labels\DeleteNoteLabelRequest;
use App\Notes\Requests\Labels\GetNoteLabelsRequest;
use App\Notes\Services\NoteLabelService;
use Infrastructure\Http\Controller;
/**
 * @group Note labels
 */
class NoteLabelController extends Controller
{
    private $noteLabelService;

    public function __construct(NoteLabelService $noteLabelService)
    {
        $this->noteLabelService = $noteLabelService;
    }

    public function index(GetNoteLabelsRequest $request, $id)
    {
        return $this->response($this->noteLabelService->index($id));
    }

    /**
     * Add label to note
     * User must be owner of both label and note.
     *
     * @urlParam id required The id of the note
     * @bodyParam id number required The id of the label to add
     */
    public function create(AddNoteLabelRequest $request, $id)
    {
        $data = $this->getRequestData($request);
        return $this->response($this->noteLabelService->create($id, $data));
    }

    /**
     * Delete label from note
     * User must be owner of both label and note.
     *
     * @urlParam id required The id of the note
     * @bodyParam id number required The id of the label to delete
     */
    public function delete(DeleteNoteLabelRequest $request, $id, $labelId)
    {
        $this->noteLabelService->delete($id, $labelId);
        return $this->response([], 204);
    }
}
