<?php


namespace App\Notes\Controllers;


use App\Notes\Requests\CreateNoteRequest;
use App\Notes\Requests\DeleteNoteRequest;
use App\Notes\Requests\GetNoteRequest;
use App\Notes\Requests\UpdateNoteRequest;
use App\Notes\Services\NoteService;
use App\Notes\Transformers\NoteTransformer;
use Infrastructure\Http\Controller;

/**
 * @group Notes
 */
class NoteController extends Controller
{
    private $noteService;

    public function __construct(NoteService $noteService)
    {
        $this->noteService = $noteService;
    }

    public function index()
    {
        return $this->response($this->noteService->index());
    }

    public function create(CreateNoteRequest $request)
    {
        $data = $request->validated();
        return $this->response($this->noteService->create($data), 201);
    }

    /**
     * Get note
     *
     * @urlParam id required The id of the note
     */
    public function read(GetNoteRequest $request, $id)
    {
        $this->parseIncludes($request);
        $transformedData = fractal($this->noteService->read($id), NoteTransformer::class);
        return $this->response($transformedData);
    }

    /**
     * Update note
     *
     * @urlParam id required The id of the note
     * @bodyParam title string Title of the note
     * @bodyParam text string Text of the note
     */
    public function update(UpdateNoteRequest $request, $id)
    {
        $data = $this->getRequestData($request);
        return $this->response($this->noteService->update($id, $data));
    }

    /**
     * Delete note
     *
     * @urlParam id required The id of the note
     */
    public function delete(DeleteNoteRequest $request, $id)
    {
        return $this->response($this->noteService->delete($id), 204);
    }
}
