<?php


namespace App\Notes\Services;


use App\Labels\Model\Label;
use App\Labels\Repositories\LabelRepository;

class NoteLabelService extends NoteServiceBase
{
    private $labelRepository;

    public function __construct(LabelRepository $labelRepository)
    {
        parent::__construct();
        $this->labelRepository = $labelRepository;
    }

    public function index($id)
    {
        $note = $this->getRequestedNote($id);
        return $note->labels;
    }

    public function create($id, array $data)
    {
        $note = $this->getRequestedNote($id);
        $label = $this->getRequestedLabel($data['id']);

        $note->labels()->save($label);
        $note->save();

        return $label;
    }

    public function delete($id, $labelId)
    {
        $note = $this->getRequestedNote($id);
        $label = $this->getRequestedLabel($labelId);

        $note->labels()->detach($label);
        $note->save();

        return $note->labels;
    }

    protected function getRequestedLabel($id) : Label
    {
        return $this->labelRepository->getWhere('id', $id)->firstOrFail();
    }
}
