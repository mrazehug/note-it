<?php


namespace App\Notes\Services;


class NoteService extends NoteServiceBase
{
    public function index()
    {
    }

    public function create(array $data)
    {
        return $this->noteRepository->create($data);
    }

    public function read($id)
    {
        return $this->getRequestedNote($id);
    }

    public function update($id, array $data)
    {
        $note = $this->getRequestedNote($id);
        return $this->noteRepository->update($note, $data);
    }

    public function delete($id)
    {
        $note = $this->getRequestedNote($id);
        $note->labels()->detach();
        $this->noteRepository->delete($note);
    }
}
