<?php


namespace App\Notes\Services;


use App\Notes\Model\Note;
use App\Notes\Repositories\NoteRepository;

class NoteServiceBase
{
    protected $noteRepository;

    public function __construct()
    {
        $this->noteRepository = new NoteRepository();
    }

    protected function getRequestedNote($id) : Note
    {
        return $this->noteRepository->getWhere('id', $id)->firstOrFail();
    }
}
