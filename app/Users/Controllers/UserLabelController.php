<?php


namespace App\Users\Controllers;


use App\Users\Requests\Labels\CreateUserLabelRequest;
use App\Users\Services\UserLabelService;
use Infrastructure\Http\Controller;
/**
 * @group User labels
 *
 * APIs for managing users
 */
class UserLabelController extends Controller
{
    private $userLabelService;

    public function __construct(UserLabelService $userLabelService)
    {
        $this->userLabelService = $userLabelService;
    }

    /**
     * Get user labels
     * Returns list of users labels.
     *
     * @urlParam id required The id of the user. Note that if you use 'current' as id, currently logged in user will be returned.
     */
    public function index($userId)
    {
        return $this->response($this->userLabelService->index($userId));
    }

    /**
     * Create user label
     * Creates label for this user.
     *
     * @urlParam id required The id of the user. Note that if you use 'current' as id, currently logged in user will be returned.
     * @bodyParam name string required Name of the label. eg. school, work, etc.
     * @bodyParam color string required Hex code of the color of this label without leading #.
     */
    public function create(CreateUserLabelRequest $request, $userId)
    {
        $data = $this->getRequestData($request);
        return $this->response($this->userLabelService->create($userId, $data), 201);
    }
}
