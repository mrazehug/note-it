<?php

namespace App\Users\Controllers;

use App\Users\Requests\UpdateUserRequest;
use Illuminate\Http\Request;
use Infrastructure\Auth\Helpers\Authentication;
use Infrastructure\Http\Controller;
use App\Users\Requests\CreateUserRequest;
use App\Users\Services\UserService;

/**
 * @group User
 *
 * APIs for managing users
 */
class UserController extends Controller
{
    private $userService;
    private $auth;

    public function __construct(UserService $userService, Authentication $auth)
    {
        $this->userService = $userService;
        $this->auth = $auth;
    }

    /**
     * Get all users
     *
     */
    public function index()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->userService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'users');

        return $this->response($parsedData);
    }

    /**
     * Get user by id
     * Creates user.
     *
     * @urlParam id required The id of the user. Note that if you use 'current' as id, currently logged in user will be returned.
     */
    public function read($id)
    {
        return $this->response($this->userService->getById($id));
    }

    /**
     * Create user
     * Note that this endpoint can be called only from inside an application. The route for creating users is register route.
     *
     */
    public function create(CreateUserRequest $request)
    {
        $data = $request->get('user', []);

        return $this->response($this->userService->create($data), 201);
    }

    /**
     * Update user
     * Updates user. Note that updating password would be done much differently in real life app.
     *
     * @urlParam id required The id of the user.
     * @bodyParam username string optional The new username of the user
     * @bodyParam email string optional The new email of the user
     * @bodyParam password string optional The new password of the user
     *
     */
    public function update(UpdateUserRequest $request, $id)
    {
        $data = $request->validated();

        return $this->response($this->userService->update($id, $data));
    }

    /**
     * Delete user
     * Deletes user.
     *
     * @urlParam id required The id of the user
     */
    public function delete($id)
    {
        return $this->response($this->userService->delete($id));
    }
}
