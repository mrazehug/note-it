<?php


namespace App\Users\Controllers;


use App\Notes\Transformers\NoteTransformer;
use App\Users\Requests\Notes\CreateUserNoteRequest;
use App\Users\Requests\Notes\DeleteUserNoteRequest;
use App\Users\Requests\Notes\GetUserNoteRequest;
use App\Users\Requests\Notes\GetUserNotesRequest;
use App\Users\Requests\Notes\UpdateUserNoteRequest;
use App\Users\Services\UserNoteService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Infrastructure\Http\Controller;
use League\Fractal\Manager;

/**
 * @group User notes
 */
class UserNoteController extends Controller
{
    private $userNoteService;

    public function __construct(UserNoteService $userNoteService)
    {
        $this->userNoteService = $userNoteService;
    }

    /**
     * Get users notes
     *
     * @urlParam id required The id of the user. Note that if you use 'current' as id, currently logged in user will be returned.
     */
    public function index(GetUserNotesRequest $request, $userId)
    {
        $this->parseIncludes($request);
        $data = $this->getRequestData($request);
        $paginatedNotes = $this->userNoteService->index($userId, $data);
        $transformedData = fractal($paginatedNotes->getCollection(), NoteTransformer::class);
        return new JsonResponse(['data' => $transformedData->toArray(), 'last_page' => $paginatedNotes->lastPage()]);
    }


    /**
     * Create user note
     *
     * @urlParam id required The id of the user. Note that if you use 'current' as id, currently logged in user will be returned.
     * @bodyParam title string required Title of the note
     * @bodyParam text string required Text of the note
     */
    public function create(CreateUserNoteRequest $request, $userId)
    {
        $data = $this->getRequestData($request);
        return $this->response($this->userNoteService->create($userId, $data), 201);
    }

    public function read(GetUserNoteRequest $request, $userId, $noteId)
    {
        return $this->response($this->userNoteService->read($userId, $noteId));
    }

    public function update(UpdateUserNoteRequest $request, $userId, $noteId)
    {
        $data = $this->getRequestData($request);
        return $this->response($this->userNoteService->update($userId, $noteId, $data));
    }

    public function delete(DeleteUserNoteRequest $request, $userId, $noteId)
    {
        return $this->response($this->userNoteService->delete($userId, $noteId), 204);
    }
}
