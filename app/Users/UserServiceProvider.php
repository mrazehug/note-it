<?php

namespace App\Users;

use App\Notes\Model\Note;
use App\Users\Models\User;
use Carbon\Laravel\ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Route;

class UserServiceProvider extends ServiceProvider
{
    private $asHimselfAbilities = [
        'create-user-note', 'get-user-notes', 'create-user-label',
        'update-user'
    ];

    private $noteAbilities = [
        'get-user-note', 'update-user-note', 'delete-user-note'
    ];

    public function boot()
    {
        $this->mapRoutes();
        $this->mapPermissions();
    }

    public function mapRoutes()
    {
        Route::group([
            'prefix' => '/api/v1',
            'middleware'=>['auth:api'],
            'namespace' => 'App\Users\Controllers'
        ], base_path('app/Users/routes.php'));
    }

    public function mapPermissions()
    {
        foreach ($this->noteAbilities as $ability)
        {
            Gate::define($ability, function (User $user, Note $note){
                return $note->canManage($user);
            });
        }

        foreach ($this->asHimselfAbilities as $ability)
        {
            Gate::define($ability, function (User $user, $userId) {
                return $user->id === $userId;
            });
        }
    }
}
