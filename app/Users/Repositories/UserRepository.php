<?php

namespace App\Users\Repositories;

use App\Users\Models\User;
use Illuminate\Support\Arr;
use Infrastructure\Database\Eloquent\Repository;

class UserRepository extends Repository
{

    public function getModel()
    {
        return new User();
    }

    public function create(array $data)
    {
        $user = $this->getModel();
        $data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);
        $user->fill($data);
        $user->save();

        return $user;
    }

    public function update(User $user, array $data)
    {
        if(Arr::has($data, 'password'))
        {
            $data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);
        }

        $user->fill($data);
        $user->save();

        return $user;
    }

    public function getWhere(...$where)
    {
        return User::where(...$where);
    }

    public function getById($id)
    {
        return $this->getWhere('id', $id);
    }

    public function delete(User $user)
    {
        $user->delete();
    }
}
