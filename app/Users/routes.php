<?php

Route::get('/users', 'UserController@index')
    ->name('users.index');
Route::get('/users/{id}', 'UserController@read')
    ->middleware('current.user.substitution')
    ->name('users.read');
Route::post('/users', 'UserController@create')
    ->name('users.create');
Route::patch('/users/{id}', 'UserController@update')
    ->middleware('current.user.substitution')
    ->name('users.update');
Route::delete('/users/{id}', 'UserController@delete')
    ->middleware('current.user.substitution')
    ->name('users.delete');

Route::get('/users/{id}/notes', 'UserNoteController@index')
    ->middleware('current.user.substitution')
    ->name('users.notes.index');
Route::post('/users/{id}/notes', 'UserNoteController@create')
    ->middleware('current.user.substitution')
    ->name('users.notes.create');

Route::get('/users/{id}/labels', 'UserLabelController@index')
    ->middleware('current.user.substitution')
    ->name('users.labels.index');
Route::post('/users/{id}/labels', 'UserLabelController@create')
    ->middleware('current.user.substitution')
    ->name('users.labels.create');
