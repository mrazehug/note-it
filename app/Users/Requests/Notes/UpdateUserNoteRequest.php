<?php


namespace App\Users\Requests\Notes;

use Infrastructure\Http\ApiRequest;

class UpdateUserNoteRequest extends ApiRequest
{
    use AuthorizeUserNoteRequest;

    public function rules()
    {
        return [
            'title' => 'sometimes',
            'text' => 'sometimes'
        ];
    }
}
