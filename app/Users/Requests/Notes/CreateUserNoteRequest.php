<?php


namespace App\Users\Requests\Notes;


use App\Notes\Model\Note;
use App\Notes\Requests\CreateNoteRequest;
use App\Users\Requests\UserBaseRequest;
use Illuminate\Support\Facades\Gate;
use Infrastructure\Http\ApiRequest;

class CreateUserNoteRequest extends UserBaseRequest
{
    public function rules()
    {
        return [
            'title' => 'required',
            'text' => 'sometimes'
        ];
    }
}
