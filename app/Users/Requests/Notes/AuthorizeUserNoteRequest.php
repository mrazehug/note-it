<?php


namespace App\Users\Requests\Notes;


use App\Notes\Model\Note;
use Illuminate\Support\Facades\Gate;

trait AuthorizeUserNoteRequest
{
    public function authorize()
    {
        $noteId = $this->route('noteId');
        return Gate::allows($this->ability, Note::where('id', $noteId)->firstOrFail());
    }
}
