<?php


namespace App\Users\Requests\Notes;


use App\Users\Requests\UserBaseRequest;

class DeleteUserNoteRequest extends UserBaseRequest
{
    use AuthorizeUserNoteRequest;
}
