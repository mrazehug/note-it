<?php


namespace App\Users\Requests\Notes;


use App\Users\Requests\UserBaseRequest;

class GetUserNoteRequest extends UserBaseRequest
{
    use AuthorizeUserNoteRequest;
}
