<?php


namespace App\Users\Requests\Notes;


use App\Users\Requests\UserBaseRequest;
use Infrastructure\Http\ApiRequest;

class GetUserNotesRequest extends UserBaseRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'labels' => 'array'
        ];
    }
}
