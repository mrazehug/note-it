<?php


namespace App\Users\Requests\Labels;

use App\Users\Requests\UserBaseRequest;

class CreateUserLabelRequest extends UserBaseRequest
{
    public function rules()
    {
        return [
            'name' => 'required',
            'color' => 'required'
        ];
    }
}

