<?php


namespace App\Users\Requests;


use Illuminate\Support\Facades\Gate;
use Infrastructure\Http\ApiRequest;

class UserBaseRequest extends ApiRequest
{
    public function authorize()
    {
        $userId = $this->route('id');
        return Gate::allows($this->ability, $userId);
    }
}
