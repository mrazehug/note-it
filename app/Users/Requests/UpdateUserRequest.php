<?php


namespace App\Users\Requests;


class UpdateUserRequest extends UserBaseRequest
{
    public function rules()
    {
        return [
            'username' => 'sometimes|unique:users',
            'email' => 'sometimes|email|unique:users',
            'password' => 'sometimes'
        ];
    }
}
