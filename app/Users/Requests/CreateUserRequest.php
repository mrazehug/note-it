<?php

namespace App\Users\Requests;

use Infrastructure\Http\ApiRequest;

class CreateUserRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => 'required|email',
            'name' => 'required|string',
            'password' => 'required|string|min:8'
        ];
    }

    public function attributes()
    {
        return [
            'email' => 'the user\'s email'
        ];
    }
}
