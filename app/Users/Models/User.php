<?php

namespace App\Users\Models;

use App\Labels\Model\Label;
use App\Notes\Model\Note;
use Illuminate\Database\Eloquent\SoftDeletes;
use Infrastructure\Auth\Model\Authenticable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticable
{
    use HasApiTokens, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'created_at'
    ];

    public function notes()
    {
        return $this->belongsToMany(Note::class, 'users_notes');
    }

    public function labels()
    {
        return $this->hasMany(Label::class);
    }
}

