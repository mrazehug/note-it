<?php

namespace App\Users\Services;

class UserService extends UserServiceBase
{
    public function getAll($options = [])
    {
        return $this->userRepository->get($options);
    }

    public function getById($userId, array $options = [])
    {
        $user = $this->getRequestedUser($userId);

        return $user;
    }

    public function create($data)
    {
        $user = $this->userRepository->create($data);

        return $user;
    }

    public function update($userId, array $data)
    {
        $user = $this->getRequestedUser($userId);

        $this->userRepository->update($user, $data);

        return $user;
    }

    public function delete($id)
    {
        $user = $this->getRequestedUser($id);

        $this->userRepository->delete($user);
    }
}
