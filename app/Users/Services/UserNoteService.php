<?php


namespace App\Users\Services;


use App\Notes\Repositories\NoteRepository;
use Illuminate\Support\Arr;

class UserNoteService extends UserServiceBase
{
    private $noteRepository;

    public function __construct(NoteRepository $noteService)
    {
        parent::__construct();
        $this->noteRepository = $noteService;
    }

    /**
     * Returns list of users notes
     * @param $userId int ID of the user
     * @param array $options Options of the query (filters etc.)
     */
    public function index($userId, array $options)
    {
        $user = $this->getRequestedUser($userId);
        // Create the note query
        $notes = $user->notes();

        // Filter labels
        if(Arr::has($options, 'labels')) {
            $notes->whereHas('labels', function ($query) use ($options) {
                return $query->whereIn('labels.id', $options['labels']);
            });
        }

        return $notes->paginate(15);
    }

    /**
     * Creates a note and assigns the user as an owner.
     * @param $userId int ID of the user
     * @param array $data Data of the note to create
     * @return \App\Notes\Model\Note
     */
    public function create($userId, array $data)
    {
        $user = $this->getRequestedUser($userId);
        $note = $this->noteRepository->create($data);

        $user->notes()->save($note);
        return $note;
    }

    public function read($userId, $noteId)
    {
        $user = $this->getRequestedUser($userId);
        return $user->notes()->where('id', $noteId)->firstOrFail();
    }

    public function update($userId, $noteId, array $data)
    {
        $user = $this->getRequestedUser($userId);
        $note = $user->notes()->where('id', $noteId)->firstOrFail();
        return $this->noteRepository->update($note, $data);
    }

    public function delete($userId, $noteId)
    {
        $user = $this->getRequestedUser($userId);
        $note = $user->notes()->where('id', $noteId)->firstOrFail();

        return $this->noteRepository->delete($note);
    }
}
