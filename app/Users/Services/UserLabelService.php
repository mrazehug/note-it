<?php


namespace App\Users\Services;


use App\Labels\Repositories\LabelRepository;

class UserLabelService extends UserServiceBase
{
    private $labelRepository;

    public function __construct(LabelRepository $labelRepository)
    {
        parent::__construct();
        $this->labelRepository = $labelRepository;
    }

    public function index($userId)
    {
        return $this->getRequestedUser($userId)->labels;
    }

    public function create($userId, array $data)
    {
        $user = $this->getRequestedUser($userId);
        $label = $this->labelRepository->create($data);

        $user->labels()->save($label);
        return $label;
    }


}
