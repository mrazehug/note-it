<?php


namespace App\Users\Services;


use App\Users\Models\User;
use App\Users\Repositories\UserRepository;

abstract class UserServiceBase
{
    protected $userRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
    }

    /**
     * Returns the user which we wish to update.
     * @param $id int ID of the user
     * @return User The user model
     */
    protected function getRequestedUser($id) : User
    {
        return $this->userRepository->getWhere('id', $id)->firstOrFail();
    }
}
