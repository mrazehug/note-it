<?php


namespace App\Users\Middleware;


use Illuminate\Http\Request;
use Infrastructure\Auth\Helpers\Authentication;

/**
 * Class CurrentUserSubstitution
 * If the route contains 'current' as id of the requested user, this middleware replaces
 * the 'current' string with id of currently logged in user.
 * @package App\Users\Middleware
 */
class CurrentUserSubstitution
{
    private $auth;

    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(Request $request, \Closure $next)
    {
        if( $request->route('id')
            && $request->route('id') == 'current')
        {
                $request->route()->setParameter('id', $this->auth->getCurrentUser()->id);
        }

        if( $request->route('userId')
            && $request->route('userId') == 'current')
        {
            $request->route()->setParameter('userId', $this->auth->getCurrentUser()->id);
        }

        return $next($request);
    }

}
