<?php


namespace App\Labels\Requests;


class CreateLabelRequest extends LabelRequestBase
{
    protected $isCreateRequest = true;

    public function rules()
    {
        return [
            'name' => 'required',
            'color' => 'required'
        ];
    }
}
