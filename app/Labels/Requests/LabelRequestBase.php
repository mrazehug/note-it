<?php


namespace App\Labels\Requests;


use App\Labels\Model\Label;
use Illuminate\Support\Facades\Gate;
use Infrastructure\Http\ApiRequest;

abstract class LabelRequestBase extends ApiRequest
{
    public function authorize()
    {
        if($this->isCreateRequest)
        {
            return true;
        }
        else
        {
            $id = $this->route('id');
            return Gate::allows($this->ability, Label::where('id', $id)->firstOrFail());
        }
    }
}
