<?php


namespace App\Labels\Requests;


class UpdateLabelRequest extends LabelRequestBase
{
    public function rules()
    {
        return [
            'name' => 'sometimes',
            'color' => 'sometimes'
        ];
    }
}
