<?php


namespace App\Labels\Controllers;


use App\Labels\Requests\CreateLabelRequest;
use App\Labels\Requests\DeleteLabelRequest;
use App\Labels\Requests\GetLabelRequest;
use App\Labels\Requests\UpdateLabelRequest;
use App\Labels\Services\LabelService;
use Infrastructure\Http\Controller;

/**
 * @group Label
 */
class LabelController extends Controller
{
    private $labelService;

    public function __construct(LabelService $labelService)
    {
        $this->labelService = $labelService;
    }

    public function index()
    {
        return $this->response($this->labelService->index());
    }

    public function create(CreateLabelRequest $request)
    {
        $data = $request->validated();
        return $this->response($this->labelService->create($data), 201);
    }

    public function read(GetLabelRequest $request, $id)
    {
        return $this->response($this->labelService->read($id));
    }

    public function update(UpdateLabelRequest $request, $id)
    {
        $data = $request->validated();
        return $this->response($this->labelService->update($id, $data));
    }

    /**
     * Delete label
     *
     * @urlParam id number required The id of the label to delete
     */
    public function delete(DeleteLabelRequest $request, $id)
    {
        $this->labelService->delete($id);
        return $this->response([], 204);
    }
}
