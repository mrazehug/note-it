<?php


namespace App\Labels\Services;


use App\Labels\Model\Label;
use App\Labels\Repositories\LabelRepository;

class LabelService
{
    private $labelRepository;

    public function __construct(LabelRepository $labelRepository)
    {
        $this->labelRepository = $labelRepository;
    }

    public function index()
    {}

    public function create(array $data)
    {
        return $this->labelRepository->create($data);
    }

    public function read($id)
    {
        return $this->getRequestedLabel($id);
    }

    public function update($id, array $data)
    {
        $label = $this->getRequestedLabel($id);
        return $this->labelRepository->update($label, $data);
    }

    public function delete($id)
    {
        $label = $this->getRequestedLabel($id);
        $label->notes()->detach();
        $this->labelRepository->delete($label);
    }

    protected function getRequestedLabel($id) : Label
    {
        return $this->labelRepository->getWhere('id', $id)->firstOrFail();
    }
}
