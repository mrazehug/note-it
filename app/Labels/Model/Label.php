<?php


namespace App\Labels\Model;


use App\Notes\Model\Note;
use App\Users\Models\User;
use Infrastructure\Database\Eloquent\ModelBase;

class Label extends ModelBase
{
    public $hidden = [ 'pivot' ];

    protected $fillable = [ 'name', 'color' ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function isOwner(User $user)
    {
        return $this->user()->where('id', $user->id)->first() != null;
    }

    public function notes()
    {
        return $this->belongsToMany(Note::class, 'notes_labels');
    }
}
