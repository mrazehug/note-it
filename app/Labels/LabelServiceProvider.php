<?php


namespace App\Labels;


use App\Labels\Model\Label;
use App\Users\Models\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class LabelServiceProvider extends ServiceProvider
{
    private $ownerAbilities = [ 'delete-label', 'get-label', 'update-label' ];

    public function boot()
    {
        $this->mapRoutes();
        $this->mapPermissions();
    }

    private function mapRoutes()
    {
        Route::group([
            'namespace'=>'App\Labels\Controllers',
            'prefix' => '/api/v1'
        ], base_path('app/Labels/routes_v1.php'));
    }

    private function mapPermissions()
    {
        foreach ($this->ownerAbilities as $ability)
        {
            Gate::define($ability, function (User $user, Label $label){
                return $label->isOwner($user);
            });
        }
    }
}
