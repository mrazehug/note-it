<?php


namespace App\Labels\Repositories;


use App\Labels\Model\Label;
use Infrastructure\Database\Eloquent\Repository;

class LabelRepository extends Repository
{
    public function getModel()
    {
        return new Label();
    }

    public function create(array $data)
    {
        $label = $this->getModel();
        $label->fill($data)->save();

        return $label;
    }

    public function update(Label $label, array $data)
    {
        $label->fill($data)->save();

        return $label;
    }

    public function delete(Label $label)
    {
        $label->delete();
    }

    public function getWhere(...$where)
    {
        return Label::where(...$where);
    }
}
