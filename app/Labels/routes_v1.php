<?php

Route::get('/labels', 'LabelController@index')
    ->middleware('auth:api')
    ->name('labels.index');
Route::post('/labels', 'LabelController@create')
    ->middleware('auth:api')
    ->name('labels.create');
Route::get('/labels/{id}', 'LabelController@read')
    ->middleware('auth:api')
    ->name('labels.read');
Route::patch('/labels/{id}', 'LabelController@update')
    ->middleware('auth:api')
    ->name('labels.update');
Route::delete('/labels/{id}', 'LabelController@delete')
    ->middleware('auth:api')
    ->name('labels.delete');
