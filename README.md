# note-it
Welcome to the note-it gitlab repository. In this readme you can find following:
 - Assignment description
 - How to develop locally
 - User manual
 - Documentation for backend
 - Documentation for API
 - Database information

### Assigment description
This assigment contains some minor changes from the original assigment. They are marginal. 
#### Features
 - User can register
 - User can login
 - User can create note, update note, delete note.
 - User can create label, delete label.

#### Implemented pages
 - Landing page for unregistered users
 - How it works page
 - Register page
 - Login page
 - Dashboard with note list. Notes may be filtered by its label.
 - Page with note detail. On this page the note can be edited.
 - Profile page. On this page are info about mail, password, etc.
 - Page for creating label

For the assigment to be accepted, all features mentioned above must be implemented.

### How to develop locally
First clone the repo. After that run ```composer install```, ```npm i``` and ```cp .env.example .env```. **Note that in this project laravel migrations 
are not used. Instead create script is used.** Said create script is located in ```database/model```. Run it. Enter your DB information into .env file.
Run ```php artisan key:generate```. Next run ```php artisan passport:install```. This produces following output:

```
Personal access client created successfully.
Client ID: <pac_id>
Client secret: <pac>
Password grant client created successfully.
Client ID: <pgc_id>
Client secret: <pgc>
```

These values needs to be copied into your .env file as following keys:
```
PERSONAL_CLIENT_ID=<pac_id>
PERSONAL_CLIENT_SECRET=<pac>
PASSWORD_CLIENT_ID=<pgc_id>
PASSWORD_CLIENT_SECRET=<pgc>
```
Finally run ```npm run dev``` to compile js. Run ```php artisan serve``` and visit ```localhost:8000```. That should be it!

##### Running tests
To run tests, run ```php artisan test```

### User manual
User manual can be found at ```https://wa.toad.cz/~mrazehug/how-it-works```

### Documentation for backend
This project uses Laravel on the backend. It should be noted that this project uses grouping by feature and not grouping by filetype philosophy.
This project uses service repository pattern. It contains following main class types:
 - Controllers
 - Services
 - Repositories
 - Models
 - Requests
 - Service providers

Let's discuss what each class type is supposed to do. We do not need to document each and every one of them because they
are so simple that they document themselves.

#### Controllers
This class accepts the requests and extracts data from it. Next it is sent to service.

#### Services
Service is where the fun happens. Service talks to other services or the repositories. Majority of the code is here.

#### Repositories
These work with the database. Each repository implements methods such as create, update, delete, getWhere etc.

#### Models
Models represent database objects. They implement relations reflecting the database structure.

#### Requests
Requests are here mainly for request validation.

#### Service providers
Service providers register routes and define abilities

### Documentation for API
Documentation for api can be found at ```https://wa.toad.cz/~mrazehug/docs/```. The trailing slash is required otherwise
style won't load. Do not understand why.

### Database
Database schema can be found in project folder in ```database/model/model.mwb```. It is mysql workbench model. Alternatively,
file ```database/model/create_script.sql``` can be reverse engineered to get the database schema.
