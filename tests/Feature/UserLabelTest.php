<?php


namespace Tests\Feature;


use App\Labels\Model\Label;
use App\Users\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class UserLabelTest extends TestCase
{
    use DatabaseTransactions;

    private $user;
    private $label;

    public function setUp(): void
    {
        parent::setUp();
        $this->label = $this->createLabel();
        $this->user = $this->createUser();
    }

    private function assignLabelOwner(Label $label = null, User $owner = null)
    {
        if($label == null) $label = $this->label;
        if($owner == null) $owner = $this->user;

        $owner->labels()->save($label);
    }

    public function testGetAll()
    {
        $this->assignLabelOwner();
        $this->assignLabelOwner($this->createLabel(), $this->user);
        $this->assignLabelOwner($this->createLabel(), $this->user);
        $this->assignLabelOwner($this->createLabel(), $this->user);
        $this->actingAs($this->user);
        $response = $this->get('/api/v1/users/current/labels');

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id', 'name', 'color'
                ]
            ]
        ]);
        $this->assertTrue(count($response->decodeResponseJson()['data']) > 3);
    }

    public function testCreate()
    {
        $this->actingAs($this->user);
        $response = $this->post('/api/v1/users/current/labels', [
            'name' => 'school',
            'color' => '#ff0000'
        ]);

        $response->assertStatus(201);
        $response->assertJsonStructure([
            'data' => [
                'id', 'name', 'color'
            ]
        ]);
    }
}
