<?php


namespace Tests\Feature;


use App\Notes\Model\Note;
use App\Users\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class UserNoteTest extends TestCase
{
    use DatabaseTransactions;

    private $user;

    private function assignNoteToUser(User $user, Note $note)
    {
        $user->notes()->save($note);
    }

    public function setUp(): void
    {
        parent::setUp();
        $this->user = $this->createUser();
        $this->actingAs($this->user);
    }

    public function testCreate()
    {
        $response = $this->post('/api/v1/users/current/notes', [
            'title' => 'A note title',
            'text' => 'A note text'
        ]);

        $response->assertStatus(201);
        $response->assertJsonStructure([
            'data' => [
                'id', 'title', 'text'
            ]
        ]);
    }

    public function testGetAll()
    {
        $this->assignNoteToUser($this->user, $this->createNote());
        $this->assignNoteToUser($this->user, $this->createNote());
        $this->assignNoteToUser($this->user, $this->createNote());

        $response = $this->get('/api/v1/users/current/notes');

        $response->assertStatus(200);
        $this->assertTrue(count($response->decodeResponseJson()['data']) >= 3);
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id', 'title', 'text'
                ]
            ]
        ]);
    }
}
