<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use DatabaseTransactions;

    public function testRegister()
    {
        $data = [
            'username' => 'unused.username',
            'email' => 'hug.mr@gmail.com',
            'password' => '12345678',
            'confirm_password' => '12345678'
        ];

        $response = $this->post('/api/v1/register', $data);

        error_log(print_r($response->decodeResponseJson(), true));

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                'email', 'username'
            ]
        ]);
    }

    public function testLogin()
    {
        $user = $this->createUser();

        $data = [
            'email' => $user->email,
            'password' => '12345678'
        ];

        $response = $this->post('/api/v1/login', $data);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                'access_token', 'expires_in'
            ]
        ]);
    }
}
