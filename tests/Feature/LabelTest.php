<?php


namespace Tests\Feature;


use App\Labels\Model\Label;
use App\Users\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class LabelTest extends TestCase
{
    use DatabaseTransactions;

    private $user;
    private $label;

    public function setUp(): void
    {
        parent::setUp();
        $this->label = $this->createLabel();
        $this->user = $this->createUser();
    }

    private function assignLabelOwner(Label $label = null, User $owner = null)
    {
        if($label == null) $label = $this->label;
        if($owner == null) $owner = $this->user;

        $owner->labels()->save($label);
    }

    public function testGetAll()
    {
        $this->assignLabelOwner();
        $this->actingAs($this->user);
        $response = $this->get('/api/v1/labels');

        $response->assertStatus(200);
    }

    public function testCreate()
    {
        $this->assignLabelOwner();
        $this->actingAs($this->user);
        $response = $this->post('/api/v1/labels', [
            'name' => 'school',
            'color' => '#ff0000'
        ]);

        $response->assertStatus(201);
        $response->assertJsonStructure([
            'data' => [
                'id', 'name', 'color'
            ]
        ]);
    }

    public function testRead()
    {
        $this->assignLabelOwner();
        $this->actingAs($this->user);
        $response = $this->get('/api/v1/labels/'.$this->label->id);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                'id', 'name', 'color'
            ]
        ]);
    }

    public function testUpdate()
    {
        $newName = 'Another name';
        $newColor = '$aaffaa';

        $this->assignLabelOwner();
        $this->actingAs($this->user);
        $response = $this->patch('/api/v1/labels/'.$this->label->id, [
            'name' => $newName,
            'color' => $newColor
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                'id', 'name', 'color'
            ]
        ]);
        $this->assertTrue($response->decodeResponseJson()['data']['name'] == $newName);
        $this->assertTrue($response->decodeResponseJson()['data']['color'] == $newColor);

    }

    public function testDelete()
    {
        $this->assignLabelOwner();
        $this->actingAs($this->user);
        $response = $this->delete('/api/v1/labels/'.$this->label->id);

        $response->assertStatus(204);
    }
}
