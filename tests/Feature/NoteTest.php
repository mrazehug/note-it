<?php


namespace Tests\Feature;


use App\Notes\Model\Note;
use App\Users\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class NoteTest extends TestCase
{
    use DatabaseTransactions;

    private $user;

    private function assignNoteToUser(User $user, Note $note)
    {
        $user->notes()->save($note);
    }

    public function setUp(): void
    {
        parent::setUp();
        $this->user = $this->createUser();
        $this->actingAs($this->user);
    }

    public function testCreate()
    {
        $this->actingAs($this->user);

        $response = $this->post('/api/v1/notes', [
            'title' => 'A note title',
            'text' => 'A note text'
        ]);

        $response->assertStatus(201);
        $response->assertJsonStructure([
            'data' => [
                'id', 'title', 'text'
            ]
        ]);
    }

    public function testUpdate()
    {
        $this->actingAs($this->user);
        $note = $this->createNote();
        $this->assignNoteToUser($this->user, $note);

        $response = $this->patch('/api/v1/notes/'.$note->id, [
            'title' => 'Modified title',
            'text' => 'Modified text'
        ]);

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'id' => $note->id,
                'title' => 'Modified title',
                'text' => 'Modified text'
            ]
        ]);
    }

    public function testUpdateUnauthorized()
    {
        $this->actingAs($this->user);
        $note = $this->createNote();
        $intruder = $this->createUser();

        $this->actingAs($intruder);
        $response = $this->patch('/api/v1/notes/'.$note->id, []);

        $response->assertStatus(403);
    }

    public function testRead()
    {
        $this->actingAs($this->user);
        $note = $this->createNote();
        $this->assignNoteToUser($this->user, $note);

        $response = $this->get('/api/v1/notes/'.$note->id);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                'id', 'title', 'text'
            ]
        ]);
    }

    public function testReadUnauthorized()
    {
        $this->actingAs($this->user);
        $note = $this->createNote();
        $intruder = $this->createUser();

        $this->actingAs($intruder);
        $response = $this->get('/api/v1/notes/'.$note->id);

        $response->assertStatus(403);
    }

    public function testDelete()
    {
        $this->actingAs($this->user);
        $note = $this->createNote();
        $this->assignNoteToUser($this->user, $note);

        $response = $this->delete('/api/v1/notes/'.$note->id);

        $response->assertStatus(204);

        $response = $this->get('/api/v1/notes/'.$note->id);
        $response->assertStatus(404);
    }

    public function testDeleteUnauthorized()
    {
        $note = $this->createNote();
        $intruder = $this->createUser();
        $this->assignNoteToUser($this->user, $note);
        $this->actingAs($intruder);

        $response = $this->delete('/api/v1/notes/'.$note->id);

        $response->assertStatus(403);
    }

    public function testGetLabels()
    {
        $note = $this->createNote();
        $label = $this->createLabel();
        $this->assignNoteToUser($this->user, $note);

        $note->labels()->save($label);

        $response = $this->get('/api/v1/notes/'.$note->id.'/labels');

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id', 'name', 'color'
                ]
            ]
        ]);
    }

    public function testAddLabel()
    {
        $note = $this->createNote();
        $label = $this->createLabel();
        $this->assignNoteToUser($this->user, $note);

        $response = $this->post('/api/v1/notes/'.$note->id.'/labels', [
            'id' => $label->id
        ]);

        $response->assertStatus(200);
    }

    public function testAddLabelUnauthorized()
    {
        $note = $this->createNote();
        $label = $this->createLabel();
        $this->assignNoteToUser($this->user, $note);
        $intruder = $this->createUser();

        $this->actingAs($intruder);
        $response = $this->post('/api/v1/notes/'.$note->id.'/labels', [
            'id' => $label->id
        ]);

        $response->assertStatus(403);
    }

    public function testDeleteLabel()
    {
        $note = $this->createNote();
        $label = $this->createLabel();
        $this->assignNoteToUser($this->user, $note);

        $note->labels()->save($label);
        $note->save();

        self::assertCount(1, $note->labels);

        $response = $this->delete('/api/v1/notes/'.$note->id.'/labels/'.$label->id);
        $note->load('labels');

        $response->assertStatus(204);
        self::assertCount(0, $note->labels);
    }

    public function testDeleteLabelUnauthorized()
    {

        $note = $this->createNote();
        $label = $this->createLabel();
        $this->assignNoteToUser($this->user, $note);
        $intruder = $this->createUser();

        $note->labels()->save($label);

        $this->actingAs($intruder);
        $response = $this->delete('/api/v1/notes/'.$note->id.'/labels/'.$label->id);

        $response->assertStatus(403);
    }
}
