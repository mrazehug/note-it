<?php

namespace Tests;

use App\Labels\Model\Label;
use App\Notes\Model\Note;
use App\Users\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Str;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function createNote() : Note
    {
        return factory(Note::class)->create();
    }

    protected function createLabel() : Label
    {
        return factory(Label::class)->create();
    }

    protected function createUser()
    {
        $user = factory(User::class)->create([
            'username' => 'hugo.mrazek',
            'email' => Str::random(16).'@gmail.com',
            'password' => password_hash('12345678', PASSWORD_BCRYPT)
        ]);

        return $user;
    }

}
