---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#Auth


APIs for managing auth.
<!-- START_8c0e48cd8efa861b308fc45872ff0837 -->
## Login
Logs in user with valid credentials.

> Example request:

```bash
curl -X POST \
    "http://localhost/api/v1/login?platform=sint" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"email":"sapiente","password":"consequatur","access_token":"laudantium"}'

```

```javascript
const url = new URL(
    "http://localhost/api/v1/login"
);

let params = {
    "platform": "sint",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "email": "sapiente",
    "password": "consequatur",
    "access_token": "laudantium"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/v1/login`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `platform` |  optional  | optional The platform using which the user should be logged in. Acceptable values are: facebook
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `email` | string |  required  | The users email
        `password` | string |  required  | The users password
        `access_token` | string |  optional  | The access_token of the third party. If platform param is filled in, this field is required.
    
<!-- END_8c0e48cd8efa861b308fc45872ff0837 -->

<!-- START_0cca7475c20114633711db04b45b4ff5 -->
## Refresh
Refreshes auth_token, if the request is sent with valid remember_token.

> Example request:

```bash
curl -X POST \
    "http://localhost/api/v1/login/refresh" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/login/refresh"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/v1/login/refresh`


<!-- END_0cca7475c20114633711db04b45b4ff5 -->

<!-- START_50f3b1266796eac6fa80ab1646687083 -->
## Attempt
Attempts to login user

> Example request:

```bash
curl -X POST \
    "http://localhost/api/v1/login/attempt" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/login/attempt"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/v1/login/attempt`


<!-- END_50f3b1266796eac6fa80ab1646687083 -->

<!-- START_8ae5d428da27b2b014dc767c2f19a813 -->
## Register
Registers user.

> Example request:

```bash
curl -X POST \
    "http://localhost/api/v1/register" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/register"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/v1/register`


<!-- END_8ae5d428da27b2b014dc767c2f19a813 -->

<!-- START_fb2ae43e2e99ff4e90f22ba03801a735 -->
## Logout
Logs user out.

> Example request:

```bash
curl -X POST \
    "http://localhost/api/v1/logout" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/logout"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/v1/logout`


<!-- END_fb2ae43e2e99ff4e90f22ba03801a735 -->

#Label


<!-- START_3693d5aae32878798944c00dd0f6ec4a -->
## Delete label

> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/v1/labels/19" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/labels/19"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/v1/labels/{id}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | number required The id of the label to delete

<!-- END_3693d5aae32878798944c00dd0f6ec4a -->

#Note labels


<!-- START_4cb25fbd7e51aa1ab53118ea3608a06b -->
## Add label to note
User must be owner of both label and note.

> Example request:

```bash
curl -X POST \
    "http://localhost/api/v1/notes/rerum/labels" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"id":1314.833338741}'

```

```javascript
const url = new URL(
    "http://localhost/api/v1/notes/rerum/labels"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "id": 1314.833338741
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/v1/notes/{id}/labels`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | The id of the note
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `id` | number |  required  | The id of the label to add
    
<!-- END_4cb25fbd7e51aa1ab53118ea3608a06b -->

<!-- START_f20ca22b99afd39e1484661036321bf7 -->
## Delete label from note
User must be owner of both label and note.

> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/v1/notes/sed/labels/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"id":61.479422357}'

```

```javascript
const url = new URL(
    "http://localhost/api/v1/notes/sed/labels/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "id": 61.479422357
}

fetch(url, {
    method: "DELETE",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/v1/notes/{id}/labels/{labelId}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | The id of the note
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `id` | number |  required  | The id of the label to delete
    
<!-- END_f20ca22b99afd39e1484661036321bf7 -->

#Notes


<!-- START_76e269bc38f4937f06266876d0d2db43 -->
## Get note

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/notes/aut" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/notes/aut"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": ""
}
```

### HTTP Request
`GET api/v1/notes/{id}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | The id of the note

<!-- END_76e269bc38f4937f06266876d0d2db43 -->

<!-- START_c25cb5dc9eb4a119ee6c99a6aceb6c42 -->
## Update note

> Example request:

```bash
curl -X PATCH \
    "http://localhost/api/v1/notes/ducimus" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"title":"necessitatibus","text":"aliquam"}'

```

```javascript
const url = new URL(
    "http://localhost/api/v1/notes/ducimus"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "title": "necessitatibus",
    "text": "aliquam"
}

fetch(url, {
    method: "PATCH",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PATCH api/v1/notes/{id}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | The id of the note
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `title` | string |  optional  | Title of the note
        `text` | string |  optional  | Text of the note
    
<!-- END_c25cb5dc9eb4a119ee6c99a6aceb6c42 -->

<!-- START_022d627e530c016903de6f3bf45b6b28 -->
## Delete note

> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/v1/notes/accusantium" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/notes/accusantium"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/v1/notes/{id}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | The id of the note

<!-- END_022d627e530c016903de6f3bf45b6b28 -->

#User


APIs for managing users
<!-- START_1aff981da377ba9a1bbc56ff8efaec0d -->
## Get all users

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/users" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/users"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": ""
}
```

### HTTP Request
`GET api/v1/users`


<!-- END_1aff981da377ba9a1bbc56ff8efaec0d -->

<!-- START_8e370f8df2793730b7d1497cb3d3a38c -->
## Get user by id
Creates user.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/users/qui" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/users/qui"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": ""
}
```

### HTTP Request
`GET api/v1/users/{id}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | The id of the user. Note that if you use 'current' as id, currently logged in user will be returned.

<!-- END_8e370f8df2793730b7d1497cb3d3a38c -->

<!-- START_4194ceb9a20b7f80b61d14d44df366b4 -->
## Create user
Note that this endpoint can be called only from inside an application. The route for creating users is register route.

> Example request:

```bash
curl -X POST \
    "http://localhost/api/v1/users" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/users"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/v1/users`


<!-- END_4194ceb9a20b7f80b61d14d44df366b4 -->

<!-- START_e7c8d66419df43f3860b7d253e2c62af -->
## Update user
Updates user. Note that updating password would be done much differently in real life app.

> Example request:

```bash
curl -X PATCH \
    "http://localhost/api/v1/users/corrupti" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"username":"est","email":"minima","password":"eum"}'

```

```javascript
const url = new URL(
    "http://localhost/api/v1/users/corrupti"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "username": "est",
    "email": "minima",
    "password": "eum"
}

fetch(url, {
    method: "PATCH",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PATCH api/v1/users/{id}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | The id of the user.
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `username` | string |  optional  | optional The new username of the user
        `email` | string |  optional  | optional The new email of the user
        `password` | string |  optional  | optional The new password of the user
    
<!-- END_e7c8d66419df43f3860b7d253e2c62af -->

<!-- START_8b97688fa48f9a3858d3b640a906b76b -->
## Delete user
Deletes user.

> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/v1/users/distinctio" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/users/distinctio"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/v1/users/{id}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | The id of the user

<!-- END_8b97688fa48f9a3858d3b640a906b76b -->

#User labels


APIs for managing users
<!-- START_5e2d5a01a55e8131c96a41ba4fabc762 -->
## Get user labels
Returns list of users labels.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/users/1/labels" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/users/1/labels"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": ""
}
```

### HTTP Request
`GET api/v1/users/{userId}/labels`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | The id of the user. Note that if you use 'current' as id, currently logged in user will be returned.

<!-- END_5e2d5a01a55e8131c96a41ba4fabc762 -->

<!-- START_8432a92694a8eeae6e3d1f0f92ace3f3 -->
## Create user label
Creates label for this user.

> Example request:

```bash
curl -X POST \
    "http://localhost/api/v1/users/1/labels" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"ab","color":"iure"}'

```

```javascript
const url = new URL(
    "http://localhost/api/v1/users/1/labels"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "ab",
    "color": "iure"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/v1/users/{userId}/labels`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | The id of the user. Note that if you use 'current' as id, currently logged in user will be returned.
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | Name of the label. eg. school, work, etc.
        `color` | string |  required  | Hex code of the color of this label without leading #.
    
<!-- END_8432a92694a8eeae6e3d1f0f92ace3f3 -->

#User notes


<!-- START_3d7cf765796c0de45a68f6d2b8b03f35 -->
## Get users notes

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/users/1/notes" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/users/1/notes"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": ""
}
```

### HTTP Request
`GET api/v1/users/{userId}/notes`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | The id of the user. Note that if you use 'current' as id, currently logged in user will be returned.

<!-- END_3d7cf765796c0de45a68f6d2b8b03f35 -->

<!-- START_907f5f246dbd273b472ac6c3a3c41941 -->
## Create user note

> Example request:

```bash
curl -X POST \
    "http://localhost/api/v1/users/1/notes" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"title":"nobis","text":"voluptatem"}'

```

```javascript
const url = new URL(
    "http://localhost/api/v1/users/1/notes"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "title": "nobis",
    "text": "voluptatem"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/v1/users/{userId}/notes`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | The id of the user. Note that if you use 'current' as id, currently logged in user will be returned.
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `title` | string |  required  | Title of the note
        `text` | string |  required  | Text of the note
    
<!-- END_907f5f246dbd273b472ac6c3a3c41941 -->


