import NoteItAxios from "~/utility/NoteItAxios";

class UserService
{
    noteItAxios

    constructor() {
        this.noteItAxios = new NoteItAxios()
    }

    async getUsers()
    {
        return this.noteItAxios.get('/users')
    }

    async getUser(id = 'current')
    {
        return this.noteItAxios.get(`/users/${id}`)
    }

    async updateUser(id = 'current', data = {})
    {
        return this.noteItAxios.patch(`/users/${id}`, data);
    }

    async deleteUser(id = 'current')
    {
        return this.noteItAxios.delete(`/users/${id}`);
    }

    async getNotes(id = 'current', options = {})
    {
        return this.noteItAxios.get(`/users/${id}/notes`, options);
    }

    async createNote(data = {})
    {
        return this.noteItAxios.post('/users/current/notes', data);
    }

    async getLabels(id = 'current', options = {})
    {
        return this.noteItAxios.get(`/users/${id}/labels`, options);
    }

    async createLabel(id = 'current', data = {})
    {
        return this.noteItAxios.post(`/users/${id}/labels`, data);
    }
}

export default UserService
