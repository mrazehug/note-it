import NoteItAxios from "~/utility/NoteItAxios";

class LabelService {
    noteItAxios

    constructor() {
        this.noteItAxios = new NoteItAxios()
    }

    deleteLabel(id)
    {
        this.noteItAxios.delete(`/labels/${id}`);
    }
}

export default LabelService
