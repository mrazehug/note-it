import NoteItAxios from "~/utility/NoteItAxios";
import camelcaseKeys from "camelcase-keys";
import {authConfig} from "~/api/authConfig"
import history from "~/utility/history";
import {to} from "~/utility/helpers";

class AuthService
{
    noteItAxios

    constructor()
    {
        this.noteItAxios = new NoteItAxios()
    }

    register(data)
    {
        return this.noteItAxios.post('/register', data);
    }

    login(data)
    {
        return this.noteItAxios.post('/login', data)
            .then(this.handleLoginSuccessfulResponse)
    }

    async attemptLogin()
    {
        return this.noteItAxios.post('/login/attempt', {})
            .then(this.handleLoginSuccessfulResponse)
            .catch((e)=>{ return false })
    }

    async refresh()
    {
        return this.noteItAxios.post('/login/refresh', {})
            .then(this.handleLoginSuccessfulResponse)
            .catch((e)=>{ return false })
    }

    handleLoginSuccessfulResponse(response)
    {
        const data = camelcaseKeys(response.data.data, {deep: true});
        authConfig.accessToken = data.accessToken;
        return true;
    }

    logout() {
        return this.noteItAxios.post('/logout', {})
            .then(()=>history.push(to('/')));
    }
}

export default AuthService
