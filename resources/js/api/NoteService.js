import NoteItAxios from "~/utility/NoteItAxios";

class NoteService {
    noteItAxios

    constructor() {
        this.noteItAxios = new NoteItAxios()
    }

    getById(id, options = {})
    {
        return this.noteItAxios.get(`/notes/${id}`, options);
    }

    update(id, data)
    {
        return this.noteItAxios.patch(`/notes/${id}`, data);
    }

    deleteNote(id) {
        return this.noteItAxios.delete(`/notes/${id}`);
    }

    addLabel(noteId, labelId) {
        return this.noteItAxios.post(`/notes/${noteId}/labels`, {
            id: labelId
        });
    }

    deleteLabel(noteId, labelId) {
        return this.noteItAxios.delete(`/notes/${noteId}/labels/${labelId}`);
    }
}

export default NoteService
