import React from 'react'
import styled from 'styled-components'
import {Layout} from "~/utility/constants";
import {ExitToApp} from "@material-ui/icons";
import useUser from "~/utility/useUser";

const LogOutButtonContainer = styled.div`
    position: fixed;
    left: -3px;
    bottom: ${Layout.DIMENSIONS.d32};
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: ${props => props.theme.bg};
    color: ${props => props.theme.fg};
    border-top: solid 1px ${props => props.theme.fg};
    border-right: solid 1px ${props => props.theme.fg};
    border-bottom: solid 1px ${props => props.theme.fg};
    border-radius: 4px;
    padding: ${Layout.DIMENSIONS.d4} ${Layout.DIMENSIONS.d4} ${Layout.DIMENSIONS.d4} ${Layout.DIMENSIONS.d8};
    cursor: pointer
`

const LogOutButton = () => {
    const {logout} = useUser();

    return <LogOutButtonContainer onClick={()=>logout()}>
        <ExitToApp/>
    </LogOutButtonContainer>
}

export default LogOutButton
