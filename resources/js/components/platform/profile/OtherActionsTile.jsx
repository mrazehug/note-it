import React, {useState} from 'react';
import Heading from "~/components/bits/Heading";
import {Col, Row} from "react-grid";
import {useForm} from "react-hook-form";
import {Layout} from "~/utility/constants";
import Button from "~/components/bits/Button";
import {t, to} from "~/utility/helpers";
import UserService from "~/api/UserService";
import {withRouter} from "react-router";
import useUser from "~/utility/useUser";

const OtherActionsTile = ({history}) => {
    const {deleteAcc} = useUser();
    const [userService] = useState(new UserService());

    return <div style={{borderBottom: "solid 1px #a1a1a1", paddingBottom: "16px"}}>
        <Row>
            <Col xs={12} md={6}>
                <Heading rank={2}>{t("Other actions")}</Heading>
                <div>
                    { t("Something more") }
                </div>
            </Col>
            <Col xs={12} md={6}>
                <div style={{paddingTop: "20px"}}>
                    <Button color={'delete'} float={"left"} onClick={()=>{
                        userService.deleteUser('current');
                        deleteAcc();
                        history.replace(to('/'))}}>{t("Delete account")}</Button>
                </div>
            </Col>
        </Row>
    </div>
}

export default withRouter(OtherActionsTile);
