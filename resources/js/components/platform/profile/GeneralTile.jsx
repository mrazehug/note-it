import React, {useState} from 'react';
import Heading from "~/components/bits/Heading";
import {Col, Container, Row} from "react-grid";
import {useForm} from "react-hook-form";
import Input from "~/components/bits/Input";
import {Layout} from "~/utility/constants";
import Button from "~/components/bits/Button";
import {t, transformFormErrors} from "~/utility/helpers";
import useUser from "~/utility/useUser";
import keys from "object-keys";
import UserService from "~/api/UserService";
import * as camelcaseKeys from "camelcase-keys";

const GeneralTile = ({user}) => {
    const {setUser} = useUser();
    const [loading, setLoading] = useState(false);
    const {register, handleSubmit, setError, errors, reset} = useForm({
        defaultValues: { ...user, password: 'I know it would not be like this' }
    });

    const onSuccess = (data) => {
        const userService = new UserService();
        setLoading(true);

        const filteredData = data;
        
        if(data.username === user.username)
            delete filteredData.username;

        if(data.email === user.email)
            delete filteredData.email;

        if(data.password === 'I know it would not be like this')
            delete filteredData.password;

        if(keys(filteredData).length === 0)
            return;

        userService.updateUser('current', filteredData)
            .then((response)=>{
                const user = camelcaseKeys(response.data.data, {deep: true});
                setUser(user);
                reset({...user, password: 'I know it would not be like this'});
                setLoading(false);
            })
            .catch((e)=>{
                const errors = transformFormErrors(camelcaseKeys(e.response.data.errors, {deep: true}));
                keys(errors).forEach((key)=>{ setError(key, {
                    type: "manual",
                    message: errors[key]
                })});
                setLoading(false);
            })
    }

    const onError = (errors) => {
        keys(errors).forEach((key)=>{
            setError(key, errors[key])
        })
    }

    return <div style={{borderBottom: "solid 1px #a1a1a1", paddingBottom: "16px"}}>
        <Row>
            <Col xs={12} md={6}>
                <Heading rank={2}>{t("General")}</Heading>
                <div>
                    { t("This is how we communicate and recognize you") }
                </div>
            </Col>
            <Col xs={12} md={6}>
                <div style={{paddingTop: "20px"}}>
                    <form onSubmit={handleSubmit(onSuccess, onError)}>
                        <Input name={"username"}
                               label={t("Username")}
                               labelSize={"large"}
                               spacing={Layout.DIMENSIONS.d8}
                               error={errors.username && errors.username.message}
                               ref={register({
                                   required: t("This field is required")
                               })}
                        />
                        <Input name={"email"}
                               label={t("Email")}
                               labelSize={"large"}
                               spacing={Layout.DIMENSIONS.d8}
                               error={errors.email && errors.email.message}
                               ref={register({
                                   required: t("This field is required")
                               })}/>
                        <Input name={"password"}
                               label={t("Password")}
                               labelSize={"large"}
                               spacing={Layout.DIMENSIONS.d8}
                               type={"password"}
                               error={errors.password && errors.password.message}
                               ref={register({
                                   required: t("This field is required")
                               })}/>
                    <Button color={'confirm'} spacingTop={Layout.DIMENSIONS.d8} float={"right"} disabled={loading}>
                        {
                            loading ? t("Working on it") : t("Save general")
                        }
                    </Button>
                    </form>
                </div>
            </Col>
        </Row>
    </div>
}

export default GeneralTile;
