import React, {useEffect, useState} from 'react';
import Heading from "~/components/bits/Heading";
import {Col, Row} from "react-grid";
import {LabelSize, languages, Layout, Themes} from "~/utility/constants";
import {NightsStay, WbSunny} from "@material-ui/icons";
import "react-toggle/style.css"
import Select from "~/components/bits/Select";
import Toggle from "~/components/bits/Toggle";
import {getLangValue, loadTheme, setLanguage, setTitle, t} from "~/utility/helpers";
import useLanguage from "~/utility/useLanguage";
import useTheme from "~/utility/useTheme";


const LookAndFeelTile = () => {
    const ul = useLanguage();
    const [selectedOption, setSelectedOption] = useState(null);
    const {switchTheme} = useTheme();

    useEffect(()=>{
        setSelectedOption(getLangValue());
    }, [])

    return <div style={{borderBottom: "solid 1px #a1a1a1", paddingBottom: "16px"}}>
        <Row>
            <Col xs={12} md={6}>
                <Heading rank={2}>{t("Look and feel")}</Heading>
                <div>
                    { t("This controls how the website looks") }
                </div>
            </Col>
            <Col xs={12} md={6}>
                <div style={{paddingTop: "20px"}}>
                    <Toggle
                        label={t("Theme")}
                        labelSize={LabelSize.large}
                        defaultChecked={loadTheme() === Themes.light}
                        icons={{
                            checked: <WbSunny style={{fontSize: "12px"}}/>,
                            unchecked: <NightsStay style={{fontSize: "12px"}}/>,
                        }}
                        spacing={Layout.DIMENSIONS.d8}
                        onChange={(e)=>switchTheme()} />
                    <Select
                        label={t("Language")}
                        labelSize={LabelSize.large}
                        spacing={Layout.DIMENSIONS.d8}
                        value={selectedOption}
                        onChange={(opt)=>{ setSelectedOption(opt); setLanguage(opt.value); ul.setLanguage(opt.value); setTitle(t('Profile')) }}
                        options={languages}
                        placeholder={""}
                    />
                </div>
            </Col>
        </Row>
    </div>
}

export default LookAndFeelTile;
