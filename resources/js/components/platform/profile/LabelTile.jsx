import React, {useEffect, useState} from 'react';
import Heading from "~/components/bits/Heading";
import {Col, Row} from "react-grid";
import "react-toggle/style.css"
import {t} from "~/utility/helpers";
import LabelChip from "~/components/platform/label/LabelChip";
import UserService from "~/api/UserService";
import * as camelcaseKeys from "camelcase-keys";
import LabelService from "~/api/LabelService";
import CreateLabelForm from "~/components/platform/label/CreateLabelForm";
import Modal from "~/components/bits/Modal";

const customStyles = {
    content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)'
    }
};

const LabelTile = () => {
    const [labels, setLabels] = useState([]);
    const [userService] = useState(new UserService());
    const [labelService] = useState(new LabelService());
    const [modalVisible, setModalVisible] = useState(false);

    useEffect(()=>{
        userService.getLabels()
            .then((response)=>{
                const labels = camelcaseKeys(response.data.data, {deep: true});
                setLabels(labels);
            })
    }, [])

    const deleteLabel = (id) => {
        labelService.deleteLabel(id);
        setLabels(labels.filter(label=>label.id !== id));
    }

    return <div style={{borderBottom: "solid 1px #a1a1a1", paddingBottom: "16px"}}>
        <Row>
            <Col xs={12} md={6}>
                <Heading rank={2}>{t("Labels")}</Heading>
                <div>
                    { t("Here you can customize your labels.") }
                </div>
            </Col>
            <Col xs={12} md={6}>
                <div style={{paddingTop: "20px"}}>
                {
                    labels.map((label)=>(
                        <LabelChip key={label.id}
                                   name={label.name}
                                   color={label.color}
                                   action={()=>deleteLabel(label.id)}/>
                    ))
                }
                    <LabelChip name={"+"}
                               color={"19AB27"}
                               action={()=>setModalVisible(true)} add/>
                </div>
            </Col>
        </Row>
        <Modal isOpen={modalVisible} style={customStyles}>
            <CreateLabelForm close={(label)=> {
                setModalVisible(false);
                if(label)
                    setLabels([...labels, label]);
            }}/>
        </Modal>
    </div>
}

export default LabelTile;
