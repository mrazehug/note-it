import React, {useEffect, useState} from "react";
import {Container} from "react-grid";
import Heading from "~/components/bits/Heading";
import GeneralTile from "~/components/platform/profile/GeneralTile";
import LookAndFeelTile from "~/components/platform/profile/LookAndFeelTile";
import {setTitle, t} from "~/utility/helpers";
import useUser from "~/utility/useUser";
import LabelTile from "~/components/platform/profile/LabelTile";
import OtherActionsTile from "~/components/platform/profile/OtherActionsTile";

const Profile = () => {
    const {user, loading} = useUser();

    useEffect(()=>{
        setTitle(t('Profile'));
    }, [])

    return <Container>
        <Heading rank={1}>{ t("Profile") }</Heading>
        {
            loading ? <div>{ t("Loading") }</div> : <>
                <GeneralTile user={user}/>
                <LookAndFeelTile/>
                <LabelTile/>
                <OtherActionsTile/>
            </>
        }
    </Container>
}

export default Profile;
