import React from 'react';
import {HeaderContainer} from "~/components/platform/layout/styles/Header.styles";
import {AccountCircle} from "@material-ui/icons";
import {withRouter} from "react-router";
import Logo from "~/components/bits/Logo";
import {Link} from "react-router-dom";
import {to} from "~/utility/helpers";

const Header = ({history}) => (
    <HeaderContainer>
        <Link to={to('/notes')}>
            <Logo size={"medium"}/>
        </Link>

        <Link to={to('/profile')}>
            <AccountCircle/>
        </Link>
    </HeaderContainer>
);

export default withRouter(Header);
