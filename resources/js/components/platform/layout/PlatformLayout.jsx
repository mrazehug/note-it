import React from 'react';
import Header from "./Header";
import {ContentContainer} from "~/components/platform/layout/styles/PlatformLayout.styles";

const PlatformLayout = (({children}) => (
    <div>
        <Header/>
        <ContentContainer>
        {
            children
        }
        </ContentContainer>
    </div>
));

export default PlatformLayout;
