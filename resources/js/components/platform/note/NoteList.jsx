import React, {useEffect, useState} from 'react'
import {Switch, Route, withRouter} from "react-router-dom";
import NoteDetail from "~/components/platform/note/NoteDetail";
import UserService from "~/api/UserService";
import camelcaseKeys from "camelcase-keys";
import NoteCard from "~/components/platform/note/NoteCard";
import {Container, Row, Col} from "react-grid";
import NoteService from "~/api/NoteService";
import {Add} from "@material-ui/icons";
import {AddContainer} from '~/components/platform/note/styles/NoteCard.style'
import {setLanguage, setTitle, t, to} from "~/utility/helpers";
import Chip from "~/components/bits/Chip";
import Box from "~/components/bits/Box";
import Heading from "~/components/bits/Heading";
import NoteFilters from "~/components/platform/note/NoteFilters";
import InfiniteScroll from "react-infinite-scroll-component";

const NoteList = ({ match, history, location }) => {
    const [notes, setNotes] = useState([]);
    const [userService] = useState(new UserService());
    const [noteService] = useState(new NoteService());
    const [labels, setLabels] = useState([]);
    const [filtersVisible, setFiltersVisible] = useState(false);
    const [currPage, setCurrPage] = useState(1);
    const [lastPage, setLastPage] = useState(99);
    const [options, setOptions] = useState({});

    const fetchNotes = (notes = []) => {
        if(currPage <= lastPage)
        {
            userService.getNotes('current', {
                include: 'labels',
                page: currPage,
                ...options
            }).then((response)=>{
                const data = camelcaseKeys(response.data, {deep: true})
                setNotes([ ...notes, ...data.data])
                setLastPage(data.lastPage);
                setCurrPage(currPage + 1);
            })
        }
    }

    useEffect(()=>{
        fetchNotes();
    }, [])

    useEffect(()=>{
        if(location.pathname === to('/notes'))
            setTitle(t('Notes'));
    }, [location.pathname])

    useEffect(()=>{
        if(filtersVisible)
        {
            userService.getLabels()
                .then((response)=>{
                    const labels = camelcaseKeys(response.data.data, {deep: true}).map((label)=>({label: label.name, value: label.id, id: label.id}));
                    setLabels(labels);
                })
        }
    }, [filtersVisible])

    useEffect(()=>{
        if(currPage === 1) {
            fetchNotes([]);
        }
    }, [currPage])

    const saveNote = (note) => {
        const index = notes.findIndex(n => n.id === note.id);
        const updatedNotes = notes;
        updatedNotes[index] = { ...note, labels: [...note.labels]};
        setNotes([...updatedNotes])
    }

    const deleteNote = (id) => {
        noteService.deleteNote(id);
        setNotes(notes.filter(note => note.id !== id));
    }

    return <>
        <Switch>
            <Route path={`${match.path}/:id`} render={(props) => <NoteDetail save={saveNote} {...props}/>}/>
        </Switch>
        <InfiniteScroll
            dataLength={notes.length} //This is important field to render the next data
            next={() => fetchNotes(notes)}
            hasMore={true}
        >
        <Container>
            <Row>
                <Col xs={12}>
                    <div>
                        <Box display={"flex"} justifyContent={"space-between"} alignItems={"center"}>
                            <Heading rank={1}>{ t("Notes") }</Heading>
                            <Chip color={"19AB27"} name={t("Filters")} action={()=>setFiltersVisible(!filtersVisible)}/>
                        </Box>
                        {
                            filtersVisible && (
                                <NoteFilters labels={labels} fetchNotes={fetchNotes} setOptions={setOptions} setCurrPage={setCurrPage}/>
                            )
                        }
                    </div>
                </Col>
                {
                    notes.map((note) => (
                        <Col  xs={12} md={6} lg={4} key={note.id}>
                            <NoteCard {...note} deleteNote={deleteNote}/>
                        </Col>
                    ))
                }
                <Col xs={12} md={6} lg={4}>
                    <AddContainer
                         onClick={()=>{
                             userService.createNote({
                                 "title": "Unnamed",
                                 "text": ""
                             }).then((response)=>{
                                 const note = camelcaseKeys(response.data.data, {deep: true});
                                 history.replace({pathname: to(`/notes/${note.id}`), state: { note }});
                                 setNotes([...notes, note]);
                             })
                         }}>
                        <Add className={"note-card-add"}/>
                    </AddContainer>
                </Col>
            </Row>
        </Container>

        </InfiniteScroll>
    </>;
}

export default withRouter(NoteList)
