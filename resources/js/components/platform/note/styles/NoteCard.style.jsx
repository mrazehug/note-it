import styled from 'styled-components';
import {Layout} from "~/utility/constants";

export const Container = styled.div`
    box-shadow: 0px 4px 8px 0px rgba(107, 107, 107, 0.25);
    width: ${(props) => props.isDetail ? '80%' : '100%' };
    height: ${(props) => props.isDetail ? '80%' : (props.isHome ? '100%' : '300px')};
    border-radius: 3px;
    display: flex;
    flex-direction: column;
    margin-bottom: 30px;
    color: #757575;
    background-color: ${(props)=>props.backgroundColor};
    max-width: ${(props) => props.isDetail ? '800px' : 'none'}
`;

export const TopPartContainer = styled.div`
    display: flex;
    justify-content: space-between;
    padding: 1em 0 1em 0;
    margin: 0 1em 0 1em;
    border-bottom: solid 1px #a1a1a1;
`;

export const LastUpdated = styled.div`
    font-size: 0.75em;
    color: #757575;
    margin-top: 0.5em;
`;

export const MiddlePartContainer = styled.div`
    padding: ${(props) => props.isDetail ? '0 1em 0 1em' : '1em 1em 1em 1em'};
    flex-grow: 1;
    overflow: hidden
`;

export const BottomPartContainer = styled.div`
    display: flex;
    justify-content: space-between;
    padding: 0.6em 0 0.6em 0;
    margin: 0 1em 0 1em;
    border-top: solid 1px #a1a1a1;
`;

export const Add = styled.div`
    font-size: 4em !important;
`

export const DetailOverlay = styled.div`
    width: 100vw;
    height: 100vh;
    background-color: rgba(0, 0, 0, 0.9);
    display: flex;
    align-items: center;
    justify-content: center;
    position: fixed;
    z-index: 100;
    top: 0
`;


export const DetailTextArea = styled.textarea`
    font-family: 'Roboto', sans-serif;
    font-size: inherit;
    width: 100%;
    height: 100%;
    box-sizing: border-box;
    resize: none;
    border-top: none;
    border-bottom: none;
    border-right: solid 1px #a1a1a1;
    border-left: solid 1px #a1a1a1;
    padding: 0.5em;
    background-color: transparent;
`;

export const AddContainer = styled(Container)`
    display: flex;
    justify-content: center;
    align-items: center
`
