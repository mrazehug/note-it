import Select from "~/components/bits/Select";
import {t} from "~/utility/helpers";
import {LabelSize, Layout} from "~/utility/constants";
import Button from "~/components/bits/Button";
import React, {useState} from "react";
import {useForm, Controller} from "react-hook-form";
import styled from 'styled-components';

const NoteFiltersContainer = styled.div`
    margin-bottom: ${Layout.DIMENSIONS.d8}
`

const NoteFilters = ({labels, fetchNotes, setOptions, setCurrPage}) => {
    const {handleSubmit, control} = useForm();
    const [filterApplied, setFilterApplied] = useState(false);

    const onSuccess = (data) => {
        const labels = data.labels.map(label=>label.value);
        if(data.labels.length > 0)
        {
            setOptions({
                labels
            });
            setCurrPage(1);
            setFilterApplied(true);
        }
        else
        {
            if(filterApplied) {
                setOptions({});
                setCurrPage(1)
                setFilterApplied(false);
            }
        }
    }

    const onError = (errors) => {
        console.error(errors);
    }

    return <form onSubmit={handleSubmit(onSuccess, onError)}>
            <Controller
                name={"labels"}
                control={control}
                render={(props)=><Select
                    label={t("Labels")}
                    labelSize={LabelSize.large}
                    spacing={Layout.DIMENSIONS.d8}
                    isMulti
                    options={labels}
                    placeholder={""}
                    {...props}
                />}
                defaultValue={[]}
            />
            <Button color={'confirm'} spacingTop={Layout.DIMENSIONS.d8} spacingBottom={Layout.DIMENSIONS.d8} float={"right"}>{ t("Run filters") }</Button>
        </form>
}

export default NoteFilters
