import React, {useEffect, useState} from "react";
import {Close, Delete, Save, TextFields} from "@material-ui/icons";
import {Link} from "react-router-dom";
import {
    BottomPartContainer,
    Container, DetailTextArea,
    LastUpdated,
    MiddlePartContainer,
    TopPartContainer
} from "~/components/platform/note/styles/NoteCard.style";
import Input from "~/components/bits/Input";
import LabelChip from "~/components/platform/label/LabelChip";
import {t, to} from "~/utility/helpers";
import Modal from "~/components/bits/Modal";
import ChooseLabel from "~/components/platform/label/ChooseLabel";
import keys from "object-keys";

const NOTE_COLORS = [
    "#ffbfe1", "#c9deff", "#ffffd4"
]

const getNoteColor = () => {
    return NOTE_COLORS[Math.floor(Math.random() * 3)];
}

let timeout = undefined;

const NoteCard = ({   id,
                      title,
                      text,
                      updatedAt,
                      isDetail = false,
                      labels,
                      save = (data) => console.warn("Save function was not set"),
                      deleteNote = (id) => console.warn("Delete note function was not set"),
                      addLabel = (noteId, labelId) => console.warn("Add label function was not set"),
                      deleteLabel = (noteId, labelId) => console.warn("Delete label function was not set"),
                      isHome,
                      saving
}) => {
    const [defaultText, setDefaultText] = useState("");
    const [defaultTitle, setDefaultTitle] = useState("");
    const [noteColor, setNoteColor] = useState('');
    const [addLabelModalVisible, setAddLabelModalVisible] = useState(false);
    const [updatedAtDate, setUpdatedAtDate] = useState(undefined)

    useEffect(()=>{
        if(isDetail)
        {
            setDefaultText(text);
            setDefaultTitle(title);
        }

        setNoteColor(getNoteColor());
    }, [])

    const getUpdatedAt = (dateString) => {
        const date = new Date(Date.parse(dateString));
        return `${date.getDate()}.${date.getMonth() + 1}.${date.getFullYear()} ${date.toTimeString().substr(0,8)}`
    }

    const handleSave = (defaultTitle, defaultText) => {
        const data = {}
        if(title !== defaultTitle)
            data.title = defaultTitle;
        if(text !== defaultText)
            data.text = defaultText;
        if(keys(data).length > 0)
            save(data);
    }

    return <Container backgroundColor={noteColor} isDetail={isDetail} isHome={isHome}>
        <TopPartContainer>
            <div>
                { isDetail ? <Input name="titleValue"
                                    value={defaultTitle}
                                    onChange={(e) => {
                                        const title = e.target.value;
                                        setDefaultTitle(title);
                                        if(timeout) clearTimeout(timeout);
                                        timeout = setTimeout(() => {
                                            handleSave(title, defaultText);
                                        }, 3000)}
                                    }
                                    dark
                /> : title}
                <LastUpdated>{t("Last updated")}&nbsp;{ getUpdatedAt(updatedAt) }</LastUpdated>
            </div>
            <div>
                {
                    saving && <Save/>
                }
                {
                    isDetail ? (
                        <Link to={to("/notes")} onClick={(e)=>{
                            if(timeout) clearTimeout(timeout);
                            save({title: defaultTitle, text: defaultText})
                        }}>
                            <Close/>
                        </Link>
                    ) : (
                        <Delete onClick={(e)=>{ if(!isDetail && !isHome) deleteNote(id) }} className={"cursor-pointer"}/>
                    )
                }
            </div>
        </TopPartContainer>
        <MiddlePartContainer isDetail={isDetail}>
            { isDetail ? <DetailTextArea name="textValue"
                                   className={"note-detail-text-area"}
                                   value={defaultText ? defaultText : ""}
                                   onChange={(e) => {
                                       const text = e.target.value;
                                       setDefaultText(text)
                                       if(timeout) clearTimeout(timeout);
                                       timeout = setTimeout(() => {
                                           handleSave(defaultTitle, text);
                                       }, 3000)}
                                   }
            /> : text}
        </MiddlePartContainer>
        <BottomPartContainer>
            {(!isDetail && !isHome) ? <Link to={{
                pathname: to(`/notes/${id}`)
            }}>
                <TextFields/>
            </Link> : <div></div>
            }
            <div>
                {
                    ( labels && labels.length > 0 && !isDetail ) && <LabelChip name={labels[0].name}
                                                    color={labels[0].color}
                                                    add={!isDetail}
                                                    action={isDetail ? ()=>deleteLabel(id, labels[0].id) : ()=>{}} isDetail/>
                }
                {
                    ( labels && labels.length > 1 && !isDetail ) && <LabelChip key={labels[0].id}
                                                    name={`+${labels.length - 1}`}
                                                    color={labels[0].color}
                                                    add={!isDetail}
                                                    action={()=>{}} plus isDetail/>
                }
                {
                    ( labels && labels && isDetail ) && labels.map((label)=><LabelChip key={label.id}
                                                                             name={label.name}
                                                                             color={label.color}
                                                                             add={!isDetail}
                                                                             action={isDetail ? ()=>deleteLabel(id, label.id) : ()=>{}}
                                                                             isDetail
                    />)
                }
                {
                    ( labels && labels.length === 0 && !isDetail ) && t("No labels.")
                }
                {
                    isDetail && <LabelChip name={"+"} color={"19AB27"} add action={()=>setAddLabelModalVisible(true)} isDetail/>
                }
            </div>
        </BottomPartContainer>
        <Modal isOpen={addLabelModalVisible}>
            <ChooseLabel selectedLabels={labels} close={()=>setAddLabelModalVisible(false)} addLabel={(labelId)=>addLabel(id, labelId)}/>
        </Modal>
    </Container>
}

export default NoteCard;
