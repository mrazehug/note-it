import React, {useEffect, useState} from "react";
import NoteCard from "~/components/platform/note/NoteCard";
import {useParams, withRouter} from "react-router-dom";
import NoteService from "~/api/NoteService";
import camelcaseKeys from "camelcase-keys";
import {DetailOverlay} from "~/components/platform/note/styles/NoteCard.style";
import {setTitle} from "~/utility/helpers";
import NotFound from "~/components/bits/NotFound";
import Unauthorized from "~/components/bits/Unauthorized";

const NoteDetail = ({ save }) => {
    const [note, setNote] = useState(undefined);
    const [loading, setLoading] = useState(true);
    // Loading is for initial loading, saving is for saving
    // after the note was edited.
    const [saving, setSaving] = useState(false);
    const { id } = useParams()
    const [noteService] = useState(new NoteService());
    const [mounted, setMounted] = useState(false);
    const [notFound, setNotFound] = useState(false);
    const [unauthorized, setUnauthorized] = useState(false);

    useEffect(()=>{
        setMounted(true);
        noteService.getById(id, {include: 'labels'})
            .then((response)=>{
                const note = camelcaseKeys(response.data.data, {deep: true});
                setNote(note);
                setLoading(false);
            })
            .catch(error => {
                if(error.response.status === 403) setUnauthorized(true);
                if(error.response.status === 404) setNotFound(true);
            })
        return () => {setMounted(false)}
    }, [])

    useEffect(()=>{
        if(note)
            setTitle(note.title);
    }, [note])

    const _save = (data) => {
        setSaving(true);
        noteService.update(id, data)
            .then((response)=>{
                const newNote = camelcaseKeys(response.data.data, {deep: true});
                if(mounted)
                    setNote({...newNote, labels: note.labels});
                save({ ...newNote, labels: note.labels });
                setSaving(false);
            });
    }

    const addLabel = (noteId, labelId) => {
        noteService.addLabel(noteId, labelId)
            .then((response) => {
                const label = camelcaseKeys(response.data.data, {deep: true});
                const updatedNote = {
                    ...note,
                    labels: [...note.labels, label]
                };
                setNote(updatedNote);

                save(updatedNote);
            })
    }

    const deleteLabel = (noteId, labelId) => {
        noteService.deleteLabel(noteId, labelId)
            .then((response)=>{
                const updatedNote = {
                    ...note,
                    labels: note.labels.filter((label)=>label.id !== labelId)
                }
                setNote(updatedNote);
                save(updatedNote);
            })
    }

    if(notFound)
        return <NotFound/>

    if(unauthorized)
        return <Unauthorized/>

    return (
        <DetailOverlay>
            {
                loading ? <div>Loading</div> : <NoteCard isDetail={true} {...note} save={_save} addLabel={addLabel} deleteLabel={deleteLabel} saving={saving}/>
            }
        </DetailOverlay>
    )
}


export default withRouter(NoteDetail);
