import styled from 'styled-components';
import {Layout} from "~/utility/constants";

export const HeadingContainer = styled.div`
    display: flex;
    justify-content: space-between;
`

export const ChooseLabelContainer = styled.div`
    min-width: 256px;
    background-color: ${props=>props.theme.bg};
    color: ${props=>props.theme.fg}
`

export const ChooseLabelItemContainer = styled.div`
    padding: ${Layout.DIMENSIONS.d4};
    display: flex;
    align-items: center;
    cursor: pointer;
`

export const ChooseLabelItemColor = styled.div`
    width: 30px;
    height: 30px;
    border-radius: 4px;
    margin-right: ${Layout.DIMENSIONS.d8};
    background-color: #${props => props.color}
`
