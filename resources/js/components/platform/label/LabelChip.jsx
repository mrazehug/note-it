import React from 'react'
import styled from 'styled-components'
import {Layout} from "~/utility/constants";
import {Clear} from "@material-ui/icons";
import {ChipContainer, ChipText} from "~/components/bits/Chip";

const LabelChipContainer = styled(ChipContainer)`
    background-color: #${props => props.color};
    padding: ${Layout.DIMENSIONS.d8};
    float: left;
    border-radius: ${Layout.DIMENSIONS.d32};
    margin: 0 ${Layout.DIMENSIONS.d8} ${props => props.isDetail ? '0' : Layout.DIMENSIONS.d4} 0;
    min-width: ${props => props.plus ? 'none' : '64px'};
    text-align: ${props => props.add ? 'center' : 'left'};
    cursor: ${props => props.add ? 'pointer' : 'auto'};
`

const LabelChipText = styled(ChipText)`
    margin-right: ${Layout.DIMENSIONS.d4};
    text-align: ${props => props.add ? 'center' : 'left'};
`

const LabelChip = ({id, name, color, action, add, plus, isDetail}) => (
    <LabelChipContainer color={color} add={add} onClick={()=>add ? action() : void(0)} plus={plus} isDetail={isDetail}>
        <LabelChipText add={add}>{ name }</LabelChipText>
        {
            !add && <Clear onClick={()=>action()} style={{fontSize: "16px", cursor: "pointer"}}/>
        }
    </LabelChipContainer>
)

export default LabelChip;
