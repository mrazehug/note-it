import React, {useState} from 'react'
import Input from "~/components/bits/Input";
import {LabelSize, Layout} from "~/utility/constants";
import {Label} from "~/components/bits/styles/Input.styles";
import styled from 'styled-components'
import ColorPicker from "~/components/bits/ColorPicker";
import {useForm, Controller} from "react-hook-form";
import Button from "~/components/bits/Button";
import UserService from "~/api/UserService";
import {Close} from "@material-ui/icons";
import {setFormErrors, t} from "~/utility/helpers";
import * as camelcaseKeys from "camelcase-keys";
import keys from "object-keys";
import {HeadingContainer} from "~/components/platform/label/styles/Label.styles";

const CreateLabelFormContainer = styled.div`
    min-width: 256px;
    background-color: ${props=>props.theme.bg};
    color: ${props=>props.theme.fg}
`

const ButtonContainer = styled.div`
    display: flex;
    justify-content: center;
    margin-top: ${Layout.DIMENSIONS.d16}
`

const CreateLabelForm = ({close}) => {
    const {register, control, handleSubmit, setError, errors} = useForm();
    const [loading, setLoading] = useState(false);
    const [userService] = useState(new UserService());

    const onSuccess = (data) => {
        setLoading(true);
        let color;
        try {
            color = data.color.hex.slice(1,7);
        }
        catch (e)
        {
            color = "000000";
        }
        userService.createLabel('current', {
            color, name: data.name
        }).then((response)=>{
            const label = camelcaseKeys(response.data.data, {deep: true});
            setLoading(false);
            close(label);
        }).catch((errors)=>{
            setFormErrors(errors, setError);
            setLoading(false);
        })
    }

    const onError = (errors) => {
        keys(errors).forEach((key)=>{
            setError(key, errors[key])
        })

        setLoading(false);
    }

    return <CreateLabelFormContainer>
        <HeadingContainer>
            <Label labelSize={LabelSize.large}>{t("Create new label")}</Label>
            <Close onClick={()=>close(null)}/>
        </HeadingContainer>
        <form onSubmit={handleSubmit(onSuccess, onError)}>
            <Input name={"name"}
                   // error={errors.name && errors.name.message}
                   label={t("Name")}
                   spacing={Layout.DIMENSIONS.d8}
                   error={errors.name && errors.name.message}
                   ref={register({
                       required: t("This field is required")
                   })}/>
            <Controller name={"color"}
                        control={control}
                        render={(props)=><ColorPicker label={t("Color")} triangle={'hide'} width={'300px'} error={errors.color && errors.color.message} {...props}/>}
                        defaultValue={"000000"}
            />
            <ButtonContainer>
                <Button color={'confirm'} type={'submit'} disabled={loading}>
                    {
                        loading ? t("Working on it") : t("Save")
                    }
                </Button>
            </ButtonContainer>
        </form>
    </CreateLabelFormContainer>;
}

export default CreateLabelForm
