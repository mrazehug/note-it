import React, {useEffect, useState} from 'react';
import UserService from "~/api/UserService";
import * as camelcaseKeys from "camelcase-keys";
import {Label} from "~/components/bits/styles/Input.styles";
import {LabelSize} from "~/utility/constants";
import {Close} from "@material-ui/icons";
import {Link} from "react-router-dom";
import {
    ChooseLabelContainer, ChooseLabelItemColor,
    ChooseLabelItemContainer,
    HeadingContainer
} from "~/components/platform/label/styles/Label.styles";
import {to} from "~/utility/helpers";

const ChooseLabel = ({selectedLabels, close, addLabel}) => {
    const [userService] = useState(new UserService());
    const [labels, setLabels] = useState([]);

    const filterUsedLabels = (labels, selectedLabels) => {
        return labels.filter((label)=>{
            try {
                selectedLabels.forEach((selectedLabel)=>{
                    if(label.id === selectedLabel.id)
                        throw Error
                })
            }
            catch (e) { return false; }
            return true;
        })
    }

    useEffect(()=>{
        userService.getLabels('current')
            .then((response)=>{
                const labels = camelcaseKeys(response.data.data, {deep: true});
                setLabels(filterUsedLabels(labels, selectedLabels));
            })
    }, [])

    useEffect(()=>{
        setLabels(filterUsedLabels(labels, selectedLabels));
    }, [selectedLabels])

    return <ChooseLabelContainer>
        <HeadingContainer>
            <Label labelSize={LabelSize.large}>Choose label to add</Label>
            <Close onClick={()=>close()}/>
        </HeadingContainer>
        {
            labels.length > 0 ? labels.map((label) => (
                    <ChooseLabelItemContainer key={label.id} onClick={()=>{ addLabel(label.id); close() }}>
                        <ChooseLabelItemColor color={label.color}/>
                        <div>{ label.name }</div>
                    </ChooseLabelItemContainer>
            )) : (
                <ChooseLabelItemContainer>
                    To create labels, visit&nbsp;<Link to={to('/profile')}>profile</Link>
                </ChooseLabelItemContainer>)
        }
    </ChooseLabelContainer>
}

export default ChooseLabel
