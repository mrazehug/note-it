import React, {useEffect} from 'react'
import {Col, Container, Row} from "react-grid";
import Heading from "~/components/bits/Heading";
import {setTitle, t, to} from "~/utility/helpers";

const HowItWorks = () => {

    useEffect(()=>{
        setTitle(t('How it works'))
    }, [])

    return <Container>
        <Heading rank={1}>{t("How it works")}</Heading>
        <Row>
            <Col xs={12}>
                <Heading rank={2}>{t("Signing up, logging in")}</Heading>
            </Col>
            <Col xs={12} md={12}>
                <p>
                    {
                        t("To sign up, visit the register page. To log in register the login page.")
                    }
                    &nbsp;
                    {
                        t("Your email, username and password can be changed later in the profile page.")
                    }
                    &nbsp;
                    {
                        t("To log out, click log out button in the bottom left corner.")
                    }
                </p>
            </Col>

            <Col xs={12}>
                <Heading rank={2}>{t("Note management")}</Heading>
            </Col>
            <Col xs={12} md={6}>
                <p>
                    {
                        t("To create a note click the square with plus on the note page.")
                    }
                    &nbsp;
                    {
                        t("Default note is created. You can start modifying it. The changes are saved automatically.")
                    }
                </p>
            </Col>
            <Col xs={12} md={6}>
                <img src={to('/images/add_note.png')} style={{float: "right"}}
                     alt={t("Image illustrating how to add note")}/>
            </Col>
            <Col xs={12} md={6}>
                <img src={to('/images/note_preview.png')} alt={t("Image illustrating note preview")}/>
            </Col>
            <Col xs={12} md={6}>
                {
                    t("To update an existing note, click the 'Tt' icon in the bottom left corner of the note preview.")
                }
                &nbsp;
                {
                    t("To delete a note, click the trash can icon in top right corner of the note preview")
                }
            </Col>

            <Col xs={12}>
                <Heading rank={2}>{t("Label management")}</Heading>
            </Col>
            <Col xs={12} md={6}>
                <p>
                    {
                        t("To create or a label, go to profile page. Here you can manage labels. Note that")
                    }
                    &nbsp;
                    {
                        t("if you delete a label, it will be removed form all notes with it.")
                    }
                </p>
            </Col>
            <Col xs={12} md={6}>
                <img src={to('/images/labels.png')} alt={t("Image illustrating how to manage labels")}/>
            </Col>

            <Col xs={12} md={6}>
                <p>
                    {
                        t("Once label is created it can be added to the note.")
                    }
                </p>
            </Col>
            <Col xs={12} md={6}>
                <img src={to('/images/add_label.png')} alt={t("Image illustrating how to add label to note")}/>
            </Col>

            <Col xs={12}>
                <Heading rank={2}>{t("Other settings")}</Heading>
            </Col>
            <Col xs={12} md={6}>
                <p>
                    {
                        t("Profile tab can be accessed by clicking the person icon in top right corner. In profile tab theme and language can be changed.")
                    }
                </p>
            </Col>
            <Col xs={12} md={6}>
                <img src={to('/images/theme_lang.png')} alt={t("Image illustrating how to switch theme or language")}/>
            </Col>


        </Row>
    </Container>
}

export default HowItWorks
