import React, {useEffect, useState} from 'react';
import {AuthActionComment, AuthContainer, AuthForm, OtherAction} from "~/components/site/auth/styles/Auth.style";
import {useForm} from "react-hook-form";
import Input from "~/components/bits/Input";
import {Container} from "react-grid";
import {Layout} from "~/utility/constants";
import Button from "~/components/bits/Button.jsx";
import Logo from "~/components/bits/Logo.jsx";
import {withRouter} from "react-router";
import AuthService from "~/api/AuthService";
import * as camelcaseKeys from "camelcase-keys";
import {setTitle, t, to, transformFormErrors} from "~/utility/helpers";
import keys from 'object-keys';
import {Link} from "react-router-dom";

const Register = ({history}) => {
    const {register, handleSubmit, setError, errors, watch} = useForm();
    const [loading, setLoading] = useState(false);

    useEffect(()=>{
        setTitle(t('Register'))
    }, [])

    const onSuccess = (data) => {
        setLoading(true);
        const authService = new AuthService();
        authService.register(data)
            .then((response)=>{
                history.push(to('/login'));
            })
            .catch((e)=>{
                const errors = transformFormErrors(camelcaseKeys(e.response.data.errors, {deep: true}));
                keys(errors).forEach((key)=>{ setError(key, {
                    type: "manual",
                    message: t(errors[key])
                })});
                setLoading(false);
            })
    }

    const onError = (errors) => {
        keys(errors).forEach((key)=>{
            setError(key, errors[key])
        })
    }

    return <AuthContainer>
        <div>
            <form onSubmit={handleSubmit(onSuccess, onError)}>
                <AuthForm>
                    <Logo size={"large"}/>
                    <AuthActionComment>
                        { t("Register with us to start taking notes") }
                    </AuthActionComment>
                    <Input name={"email"}
                           label={t("Email")}
                           error={errors.email && errors.email.message}
                           ref={register({
                               required: t("This field is required"),
                           })}
                           spacing={Layout.DIMENSIONS.d8}/>
                    <Input name={"username"}
                           label={t("Username")}
                           error={errors.username && errors.username.message}
                           ref={register({
                               required: t("This field is required"),
                           })}
                           spacing={Layout.DIMENSIONS.d8}/>
                    <Input name={"password"}
                           label={t("Password")}
                           error={errors.password && errors.password.message}
                           type={"password"}
                           ref={register({
                               required: t("This field is required"),
                               minLength: {
                                   value: 8, message: t("Min length is 8")
                               }
                           })}
                           spacing={Layout.DIMENSIONS.d8}/>
                    <Input name={"confirm_password"}
                           label={t("Confirm password")}
                           error={errors.confirm_password && errors.confirm_password.message}
                           type={"password"}
                           ref={register({
                               required: t("This field is required"),
                               validate: value =>
                                   value === watch('password') || t("The passwords do not match")
                           })}
                           spacing={Layout.DIMENSIONS.d8}/>
                    <Button type={"submit"} spacingTop={Layout.DIMENSIONS.d4} color={'confirm'} disabled={loading}>
                        {
                            loading ? t("Working on it") : t("Register")
                        }
                    </Button>
                </AuthForm>
            </form>
            <OtherAction>{ t('Already registered?') }&nbsp;<Link to={to('/login')}>{t('Log in')}</Link></OtherAction>
        </div>
    </AuthContainer>
}

export default withRouter(Register);
