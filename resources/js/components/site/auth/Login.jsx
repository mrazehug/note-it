import {useForm} from "react-hook-form";
import React, {useEffect, useState} from "react";
import AuthService from "~/api/AuthService";
import {setTitle, t, to, transformFormErrors} from "~/utility/helpers";
import * as camelcaseKeys from "camelcase-keys";
import keys from "object-keys";
import {
    AuthActionComment,
    AuthContainer,
    AuthForm,
    OtherAction,
    UnauthorizedError
} from "~/components/site/auth/styles/Auth.style";
import Logo from "~/components/bits/Logo.jsx";
import Input from "~/components/bits/Input";
import {Layout} from "~/utility/constants";
import Button from "~/components/bits/Button.jsx";
import {authConfig} from "~/api/authConfig";
import useUser from "~/utility/useUser";
import {Link} from "react-router-dom";

const Login = ({history}) => {
    const {register, handleSubmit, setError, clearErrors, errors, watch} = useForm();
    const [loading, setLoading] = useState(false);
    const {setLoggedIn} = useUser();

    useEffect(()=>{
        clearErrors('unauthorized');
    }, [watch('email'), watch('password')])

    useEffect(()=>{
        setTitle(t('Log in'))
    }, [])

    const onSuccess = (data) => {
        setLoading(true);
        const authService = new AuthService();
        authService.login(data)
            .then((response)=>{
                setLoggedIn(true);
                history.replace(to('/notes'));
            })
            .catch((e)=>{
                if(e.response.status === 401)
                    setError('unauthorized', {
                        type: 'manual',
                        message: 'Email, username or password is wrong'
                    })
                else
                {
                    const errors = transformFormErrors(camelcaseKeys(e.response.data.errors, {deep: true}));
                    keys(errors).forEach((key)=>{ setError(key, {
                        type: "manual",
                        message: t(errors[key])
                    })});
                }

                setLoading(false);
            })
    }

    const onError = (errors) => {
        keys(errors).forEach((key)=>{
            setError(key, errors[key])
        })

        setLoading(false);
    }

    return <AuthContainer>
        <div>
            <form onSubmit={handleSubmit(onSuccess, onError)}>
                <AuthForm>
                    <Logo size={"large"}/>
                    <AuthActionComment>
                        { t("Login to start taking notes") }
                    </AuthActionComment>
                    {
                        errors.unauthorized &&
                        <UnauthorizedError>{errors.unauthorized.message}</UnauthorizedError>
                    }
                    <Input name={"email"}
                           label={t("Email or username")}
                           error={errors.email && errors.email.message}
                           ref={register({
                               required: t("This field is required")
                           })}
                           spacing={Layout.DIMENSIONS.d8}/>
                    <Input name={"password"}
                           label={t("Password")}
                           error={errors.password && errors.password.message}
                           type={"password"}
                           ref={register({
                               required: t("This field is required")
                           })}
                           spacing={Layout.DIMENSIONS.d8}/>
                    <Button type={"submit"} spacingTop={Layout.DIMENSIONS.d4} color={'confirm'} disabled={loading}>
                        {
                            loading ? t("Working on it") : t("Log in")
                        }
                    </Button>
                </AuthForm>
            </form>
            <OtherAction>{ t('Not yet registered?') }&nbsp;<Link to={to('/register')}>{t('Register')}</Link></OtherAction>
        </div>
    </AuthContainer>
}

export default Login;
