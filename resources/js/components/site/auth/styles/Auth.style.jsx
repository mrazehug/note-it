import styled from 'styled-components'
import {Layout} from "~/utility/constants";

export const AuthContainer = styled.div`
    display:flex;
    justify-content: center;
    align-items: center;
    height: calc(100vh - ${Layout.HEADER_HEIGHT})
`;

export const AuthForm = styled.div`
    display: flex;
    flex-direction: column;
    border: 1px solid #a1a1a1;
    border-radius: 3px;
    padding: ${Layout.DIMENSIONS.d16};
    background-color: ${props => props.theme.bg}
`

export const OtherAction = styled.div`
    font-size: 0.8em;
    margin-top: ${Layout.DIMENSIONS.d8};
    border: 1px solid #a1a1a1;
    border-radius: 3px;
    padding: ${Layout.DIMENSIONS.d16};
    text-align: center
`

export const AuthActionComment = styled.div`
    color: #a1a1a1;
    text-align: center;
    padding-bottom: ${Layout.DIMENSIONS.d8}
`

export const UnauthorizedError = styled.div`
    color: red;
    text-align: center;
    font-size: 0.8em
`
