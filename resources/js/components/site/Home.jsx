import React, {useEffect, useState} from "react";
import {Col, Container, Row} from "react-grid";
import NoteCard from "~/components/platform/note/NoteCard";
import styled from 'styled-components'
import {Layout} from "~/utility/constants";
import Heading from "~/components/bits/Heading";
import {Link} from "react-router-dom";
import Button from "~/components/bits/Button";
import {HeaderContainer} from "~/components/site/layout/styles/Header.styles";
import {setTitle, t} from "~/utility/helpers";

const FirstSection = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    min-height: calc(100vh - ${Layout.HEADER_HEIGHT} - ${Layout.CONTENT_TOP_PADDING});
`

const NotesContainer = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
`

const FancyTextContainer = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    height: 100%;
`

const BackNoteContainer = styled.div`
    height: 350px;
    width: 350px;
    margin-left: ${props => props.position === 'right' ? '-70px' : 0};
    margin-right: ${props => props.position === 'left' ? '-70px' : 0};
    z-index: 99
`

const FrontNoteContainer = styled.div`
    height: 400px;
    width: 450px;
    z-index: 100;
    margin-right: -60px;
    margin-left: -60px
`

let currentTextIndex = 0;
let currentCarretIndex = 0;
const texts = [
    t('Note anything.'), t('Note anywhere.'), t('Note anytime.')
]
let speed = 50;

let id;
let tick;
let updated = new Date().toISOString();

function useInterval(callback, delay) {
    // Remember the latest callback.
    const savedCallback = React.useRef();
    React.useEffect(() => {
        savedCallback.current = callback;
    }, [callback]);
    // Set up the interval.
    React.useEffect(() => {
        tick = () => {
            savedCallback.current();
        }
        if (delay !== null) {
            id = setInterval(tick, delay);
            return () => clearInterval(id);
        }
    }, [delay]);
}

const Home = () => {
    const [frontNoteText, setFrontNoteText] = useState("");

    useInterval(()=>{
        setFrontNoteText(frontNoteText + texts[currentTextIndex][currentCarretIndex]);
        currentCarretIndex++;


        if(currentCarretIndex === texts[currentTextIndex].length)
        {
            currentCarretIndex = 0;
            currentTextIndex++;
            if(currentTextIndex === texts.length)
                currentTextIndex = 0;
            updated = new Date().toISOString();
            clearInterval(id)
            setTimeout(()=>{
                setFrontNoteText("");
                id = setInterval(tick, speed);
            }, 1500)
        }
    }, speed)

    useEffect(()=>{
        setTitle();
        return () => {
            if(id) clearInterval(id);
        }
    }, [])

    return <FirstSection>
        <Container>
            <Row>
                <Col xs={12} lg={6}>
                    <FancyTextContainer>
                    <Heading rank={1}>{ t("The definitive app for taking notes.") }</Heading>
                    <Heading rank={3}>
                        { t(`Insert cheesy startup line here. Could also brag about who uses us. Wrote more text so the design looks as I want it to look, lol. Another compilation later it is still not long enough. There you go. Sort of.`) }
                    </Heading>
                    </FancyTextContainer>
                </Col>
                <Col xs={12} lg={6}>
                    <NotesContainer>
                        <BackNoteContainer position={'left'}>
                            <NoteCard title={"Note"} text={"Very much text, yes"} isHome={true} updatedAt={updated}/>
                        </BackNoteContainer>
                        <FrontNoteContainer>
                            <NoteCard title={"note-it"} text={frontNoteText} isHome={true} updatedAt={updated}/>
                        </FrontNoteContainer>
                        <BackNoteContainer position={'right'}>
                            <NoteCard title={"BackNote"} text={"Very much text, yes"} isHome={true} updatedAt={updated}/>
                        </BackNoteContainer>
                    </NotesContainer>
                </Col>
            </Row>
        </Container>
    </FirstSection>
}

export default Home
