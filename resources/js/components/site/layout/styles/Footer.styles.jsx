import styled from "styled-components";
import {Layout} from "~/utility/constants";


export const FooterContainer = styled.div`
    background-color: ${props => props.theme.bg};
    color: ${props => props.theme.fg};
    padding-bottom: 98px;
    border-top: solid 1px ${props => props.theme.borderColor}
`
