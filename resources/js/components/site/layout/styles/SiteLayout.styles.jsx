import styled from "styled-components";
import {Layout} from "~/utility/constants";

export const ContentContainer = styled.div`
    padding-top: calc(${Layout.HEADER_HEIGHT} + ${Layout.CONTENT_TOP_PADDING});
    min-height: calc(100vh - ${Layout.HEADER_HEIGHT} - ${Layout.CONTENT_TOP_PADDING});
    color: ${props => props.theme.fg};
    background-color: ${props => props.theme.bg}
`
