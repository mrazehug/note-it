import styled from 'styled-components'
import {Layout} from "~/utility/constants";


export const HeaderContainer = styled.div`
    position: fixed;
    display: flex;
    justify-content: space-between;
    width: 100%;
    box-sizing: border-box;
    top: 0;
    height: ${Layout.HEADER_HEIGHT};
    border-bottom: solid 1px #c2c2c2;
    z-index: 99;
    background-color: ${props => props.theme.bg};
    color: ${props => props.theme.fg};
    align-items: center;
    padding-right: ${Layout.DIMENSIONS.d8}
`

export const MenuContainer = styled.div`
    display: flex;
    align-items: center
`

export const MenuItem = styled.div`
    padding: 8px
`
