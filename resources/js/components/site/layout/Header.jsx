import React from 'react';
import {AccountCircle} from "@material-ui/icons";
import {withRouter} from "react-router";
import {HeaderContainer, MenuContainer, MenuItem} from "~/components/site/layout/styles/Header.styles";
import {Link} from "react-router-dom";
import Logo from "~/components/bits/Logo";
import Button from "~/components/bits/Button";
import {Layout} from "~/utility/constants";
import {t, to} from "~/utility/helpers";

const Header = () => (
    <HeaderContainer>
        <MenuContainer>
            <Link to={to('/')}>
                <Logo size={"medium"}/>
            </Link>
            <MenuItem>
            <Link to={to('/how-it-works')}>
                {
                    t("How it works")
                }
            </Link></MenuItem>
        </MenuContainer>

        <div>
            <Link to={to('/login')}>
                <Button spacingRight={Layout.DIMENSIONS.d4} color={'confirm'}>{ t("Log in") }</Button>
            </Link>
            <Link to={to('/register')}>
                <Button color={'confirm'}>{ t("Register") }</Button>
            </Link>
        </div>
    </HeaderContainer>
);

export default withRouter(Header);
