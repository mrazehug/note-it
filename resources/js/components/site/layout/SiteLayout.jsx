import React from 'react';
import {ContentContainer} from "~/components/site/layout/styles/SiteLayout.styles";
import Header from "~/components/site/layout/Header";
import Footer from "~/components/site/layout/Footer";

const SiteLayout = (({children}) => (
    <div>
        <Header/>
        <ContentContainer>
            {
                children
            }
        </ContentContainer>
        <Footer/>
    </div>
));

export default SiteLayout;
