import {Col, Container, Row} from "react-grid";
import Toggle from "~/components/bits/Toggle";
import {getLangValue, loadTheme, setLanguage, t} from "~/utility/helpers";
import {LabelSize, languages, Layout, Themes} from "~/utility/constants";
import {NightsStay, WbSunny} from "@material-ui/icons";
import Select from "~/components/bits/Select";
import React, {useState} from "react";
import useLanguage from "~/utility/useLanguage";
import useTheme from "~/utility/useTheme";
import {FooterContainer} from "~/components/site/layout/styles/Footer.styles";
import Logo from "~/components/bits/Logo";

const Footer = () => {
    const ul = useLanguage();
    const [selectedOption, setSelectedOption] = useState(getLangValue());
    const {switchTheme} = useTheme();

    return<FooterContainer>
    <Container>
        <Row>
            <Col xs={12} md={6}>
                <Logo size={'large'} align={'left'}/>
            </Col>
            <Col xs={12} md={6}>
                <div style={{paddingTop: "20px"}}>
                    <Toggle
                        label={t("Theme")}
                        labelSize={LabelSize.large}
                        defaultChecked={loadTheme() === Themes.light}
                        icons={{
                            checked: <WbSunny style={{fontSize: "12px"}}/>,
                            unchecked: <NightsStay style={{fontSize: "12px"}}/>,
                        }}
                        spacing={Layout.DIMENSIONS.d8}
                        onChange={(e) => switchTheme()}/>
                    <Select
                        label={t("Language")}
                        labelSize={LabelSize.large}
                        spacing={Layout.DIMENSIONS.d8}
                        value={selectedOption}
                        onChange={(opt) => {
                            setSelectedOption(opt);
                            setLanguage(opt.value);
                            ul.setLanguage(opt.value)
                        }}
                        options={languages}
                        placeholder={""}
                    />
                </div>
            </Col>
        </Row>
    </Container>
    </FooterContainer>
}

export default Footer;
