import React, {useEffect, useState} from "react";
import ReactDOM from "react-dom";
import PlatformLayout from "~/components/platform/layout/PlatformLayout";
import SiteLayout from "~/components/site/layout/SiteLayout";

import {
    Switch,
    Route,
    Router
} from "react-router-dom";

import NoteList from "~/components/platform/note/NoteList";
import AuthService from "~/api/AuthService";
import history from "~/utility/history";
import Home from "~/components/site/Home";
import Register from "~/components/site/auth/Register";
import Login from "~/components/site/auth/Login.jsx";
import {authConfig} from "~/api/authConfig";
import Profile from "~/components/platform/profile/Profile";
import useLanguage, {ProvideLanguage} from "~/utility/useLanguage";
import {ThemeProvider} from "styled-components";
import useTheme, {ProvideTheme} from "~/utility/useTheme";
import useUser, {ProvideUser} from "~/utility/useUser";
import LogOutButton from "~/components/platform/LogOutButton";
import HowItWorks from "~/components/site/HowItWorks";
import {setTitle, to} from "~/utility/helpers";
import Loading from "~/components/bits/Loading";
import NotFound from "~/components/bits/NotFound";

const App = () => {
    const {loading, loggedIn, attemptLogin} = useUser();
    const {language} = useLanguage();
    const {theme} = useTheme()

    useEffect(()=>{
        attemptLogin();
    }, [])

    useEffect(()=>{
        if(loggedIn && window.location.pathname === to('/')) {
            history.push(to('/notes'));
            setTitle('notes');
        }
    }, [loggedIn])

    useEffect(()=>{
    }, [language])

    return (
        <ThemeProvider theme={theme}>
            <Router history={history}>
                {
                    loading ? <Loading/> : (
                        <>
                            {
                                loggedIn ?
                                    <PlatformLayout>
                                        <LogOutButton/>
                                        <Switch>
                                            <Route path={to('/notes')} component={NoteList}/>
                                            <Route path={to('/profile')} component={Profile}/>

                                            <Route component={NotFound}/>
                                        </Switch>
                                    </PlatformLayout>
                                    :
                                    <SiteLayout>
                                        <Switch>
                                            <Route exact path={to('/')} component={Home}/>
                                            <Route path={to('/register')} component={Register}/>
                                            <Route path={to('/login')} component={Login}/>
                                            <Route exact path={to('/how-it-works')} component={HowItWorks}/>

                                            <Route component={NotFound}/>
                                        </Switch>
                                    </SiteLayout>
                            }
                        </>
                    )
                }
            </Router>
        </ThemeProvider>
    )
}

export default App


if(document.getElementById("root"))
{
    ReactDOM.render(
        <ProvideTheme>
            <ProvideUser>
                <ProvideLanguage>
                    <App/>
                </ProvideLanguage>
            </ProvideUser>
        </ProvideTheme>, document.getElementById("root"));
}
