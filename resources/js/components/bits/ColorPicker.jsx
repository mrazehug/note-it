import React from 'react'
import {TwitterPicker} from "react-color";
import LE from "~/components/bits/LE";

const ColorPicker = React.forwardRef(({label, labelSize, error, value, onChange ,...props}, ref) => (
    <>
        <LE label={label} labelSize={labelSize} error={error}/>
        <TwitterPicker color={value} triangle={'hide'} width={'300px'} onChange={onChange} ref={ref}/>
    </>
));

export default ColorPicker;
