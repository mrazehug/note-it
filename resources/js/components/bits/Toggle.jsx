import React from 'react'
import ReactToggle from 'react-toggle'
import LE from "~/components/bits/LE";
import styled from 'styled-components';

const ToggleContainer = styled.div`
    margin-bottom: ${props => props.spacing ? props.spacing : "0px"}
`

const Toggle = ({label, labelSize, error, spacing, ...props}) => (
    <ToggleContainer spacing={spacing}>
        <LE label={label} labelSize={labelSize} error={error}/>
        <ReactToggle {...props}/>
    </ToggleContainer>
)

export default Toggle;
