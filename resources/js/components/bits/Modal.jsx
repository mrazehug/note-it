import React, {useEffect} from 'react'
import {Row} from "react-grid";
import CreateLabelForm from "~/components/platform/label/CreateLabelForm";
import useTheme from "~/utility/useTheme";
import ReactModal from 'react-modal';

const customStyles = (theme) => ({
    content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)',
        backgroundColor       : `${theme.bg}`
    },
    overlay: {
        zIndex: 9999
    }
});

const Modal = ({children, isOpen, ...props}) => {
    const {theme} = useTheme();

    useEffect(()=>{
        ReactModal.setAppElement(document.getElementById('root'));
    }, [])

    return <ReactModal isOpen={isOpen} style={customStyles(theme)}>
        {
            children
        }
    </ReactModal>
}

export default Modal;
