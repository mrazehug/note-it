import styled from "styled-components";
import {Layout} from "~/utility/constants";
const handleLabelSize = (labelSize) => {
    switch (labelSize)
    {
        case "large": return "1.0em"
        default: return "0.8em"
    }
}
export const LEContainer = styled.div`
    margin-bottom: ${Layout.DIMENSIONS.d8};
    display: flex
`
export const Error = styled.div`
    font-size: ${(props) => handleLabelSize(props.labelSize)};
    color: red;
    margin-left: ${Layout.DIMENSIONS.d4}
`

export const Label = styled.label`
    font-size: ${(props) => handleLabelSize(props.labelSize)};
`

export const ErrorContainer = styled.div`
    height: calc(100vh - ${Layout.HEADER_HEIGHT});
    width: 100vw;
    top: ${Layout.HEADER_HEIGHT};
    left: 0;
    display: flex;
    position:fixed;
    background-color: ${props => props.theme.bg};
    justify-content: center;
    align-items: center;
    z-index: 99999
`

export const ErrorHeading = styled.h1`
    font-size: 8em;
    text-align: center
`
