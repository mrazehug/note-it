import React from 'react'

const Heading = ({children, rank}) => {
    switch (rank)
    {
        case 1:
            return <h1>{ children }</h1>
        case 2:
            return <h2>{ children }</h2>
        case 3:
            return <h3>{ children }</h3>
    }
}

export default Heading
