import React from "react";
import Heading from "~/components/bits/Heading";
import {t} from "~/utility/helpers";
import {ErrorContainer, ErrorHeading} from "~/components/bits/styles/Input.styles";

export const NotFound = () => (
    <ErrorContainer>
        <div>
        <ErrorHeading>404</ErrorHeading>
        <Heading rank={2}>{t("Could not find what you were looking for")}</Heading>
        </div>
    </ErrorContainer>
)

export default NotFound
