import React from 'react';
import ReactSelect from 'react-select';
import LE from "~/components/bits/LE";
import { withTheme } from 'styled-components';

const customStyles = {
    control: (provided, state) => ({
        ...provided,
        border: `solid 1px ${state.theme.borderColor}`,
        backgroundColor: state.theme.bg,
        borderRadius: '3px'
    }),
    container: (provided, state) => ({
        ...provided,
        marginBottom: state.spacing,
        backgroundColor: state.theme.bg
    }),
    singleValue: (provided, state) => ({
        ...provided,
        color: `${state.theme.fg} !important`
    }),
    menu: (provided, state) => ({
        ...provided,
        backgroundColor: state.theme.bg
    }),
    option: (provided, state) => ({
        ...provided,
        backgroundColor: state.isFocused ? state.theme.accent : state.theme.bg,
        color: state.theme.fg
    })
}

const Select = React.forwardRef(({label, labelSize, error, isMulti, ...props}, ref) => (
    <>
        <LE label={label} labelSize={labelSize} error={error}/>
        <ReactSelect styles={customStyles} isMulti={isMulti} ref={ref} {...props}/>
    </>
))

export default withTheme(Select)
