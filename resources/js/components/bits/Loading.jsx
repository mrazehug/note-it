import React from 'react'
import Loader from 'react-loader-spinner'
import styled from 'styled-components'

const LoadingContainer = styled.div`
    position: fixed;
    height: 100vh;
    width: 100vw;
    background-color: ${props => props.theme.bg};
    display: flex;
    justify-content: center;
    align-items: center
`

const Loading = () => (
    <LoadingContainer>
        <Loader
            type="Puff"
            color="#00BFFF"
            height={100}
            width={100}
            timeout={3000} //3 secs
        />
    </LoadingContainer>
)

export default Loading;
