import React from 'react'
import styled from 'styled-components'
import {Layout} from "~/utility/constants";

const StyledLogo = styled.div`
    font-size: ${props => {
    switch (props.size) {
        case 'large':
            return "36px";
        case 'medium':
            return "24px";
        case 'small':
            return "12px";
    }

}};
    text-align: ${props => props.align ? props.align : 'center'};
    padding: ${Layout.DIMENSIONS.d8};
    cursor: pointer
`

const Logo = ({size, align}) => (
    <StyledLogo size={size} align={align}>note-it</StyledLogo>
)

export default Logo;
