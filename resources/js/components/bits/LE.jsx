import React from 'react'
import {Error, Label, LEContainer} from "~/components/bits/styles/Input.styles";

const LE = ({label, labelSize, error, name}) => (
    <>
        {
            (label || error) &&
            <LEContainer>
                {
                    label && <Label htmlFor={name} labelSize={labelSize}>{label}</Label>
                }
                {
                    error && <Error labelSize={labelSize}>{error}</Error>
                }
            </LEContainer>
        }
    </>
);

export default LE;
