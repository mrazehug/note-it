import React from 'react'
import styled from 'styled-components'

const StyledBox = styled.div`
    display: ${props => props.display};
    padding: ${props => props.padding};
    justify-content: ${props => props.justifyContent};
    align-items: ${props => props.alignItems};
`

const Box = ({children, ...props}) => (
    <StyledBox {...props}>
        {
            children
        }
    </StyledBox>
)

export default Box;
