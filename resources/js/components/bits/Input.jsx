import React from 'react'
import styled from 'styled-components'
import LE from "~/components/bits/LE";

const StyledInput = styled.input`
    background-color: transparent;
    padding: 0.5em;
    border: solid 1px ${props=> props.error ? 'red' : '#a1a1a1'};
    border-radius: 3px;
    margin-bottom: ${props => props.spacing};
    width: 100%;
    box-sizing: border-box;
    height: 38px;
    color: ${props => props.dark ? '#3636360' : props.theme.fg}
`

const Input = React.forwardRef(({value, onChange, label, labelSize, error, dark, ...props}, ref) => (
    <div>
        <LE label={label} labelSize={labelSize} error={error} {...props}/>
        <StyledInput value={value} onChange={onChange} error={error} ref={ref} dark={dark} {...props}/>
    </div>
))

export default Input;
