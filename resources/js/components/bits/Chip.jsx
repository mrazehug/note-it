import React from 'react'
import styled from 'styled-components'
import {Layout} from "~/utility/constants";
import {Clear as MUIClear} from "@material-ui/icons";

export const ChipContainer = styled.div`
    background-color: #${props => props.color};
    padding: ${Layout.DIMENSIONS.d8};
    border-radius: ${Layout.DIMENSIONS.d32};
    display: flex;
    align-items: center;
    margin: 0 ${Layout.DIMENSIONS.d8} 0 0;
    text-align: left;
    cursor: pointer;
    color: white
`

export const ChipText = styled.div`
    margin-right: ${Layout.DIMENSIONS.d4};
    text-align: left;
    width: 100%
`

export const Clear = styled(MUIClear)`
    font-size: 16px;
    cursor: pointer
`

const Chip = ({id, name, color, action, deletable}) => (
    <ChipContainer color={color} onClick={()=>{
        if(action && !deletable)
            action();
    }}>
        <ChipText>{ name }</ChipText>
        {
            ( action && deletable ) && <Clear onClick={()=>action()}/>
        }
    </ChipContainer>
)

export default Chip;
