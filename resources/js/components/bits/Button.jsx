import React from 'react'
import styled from 'styled-components'

const handleColorType = (color) => {
    switch (color)
    {
        case 'primary':
            return '#c0e9fa';
        case 'confirm':
            return '#19AB27';
        case 'delete':
            return '#cf0015'
        default:
            return 'blue'
    }
}

const StyledButton = styled.button`
    background-color: ${props => props.isPrimary ? '#c0e9fa' : handleColorType(props.color)};
    text-transform: uppercase;
    padding: 0.5em;
    text-align: center;
    border-radius: 3px;
    border: none;
    margin-top: ${props => props.spacingTop ? props.spacingTop : "0"};
    margin-right: ${props => props.spacingRight ? props.spacingRight: "0"};
    margin-bottom: ${props => props.spacingBottom ? props.spacingBottom : "0"};
    float: ${props => props.float ? props.float : "left"};
    cursor: pointer;
    color: white

`

const Button = ({isPrimary, children, spacingTop, spacingBottom, color, ...props}) => (
    <StyledButton isPrimary={isPrimary} color={color} spacingTop={spacingTop} spacingBottom={spacingBottom} {...props}>{ children }</StyledButton>
)

export default Button
