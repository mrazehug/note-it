import React from "react";
import Heading from "~/components/bits/Heading";
import {t} from "~/utility/helpers";
import {ErrorContainer, ErrorHeading} from "~/components/bits/styles/Input.styles";

export const Unauthorized = () => (
    <ErrorContainer>
        <div>
            <ErrorHeading>403</ErrorHeading>
            <Heading rank={2}>{t("Wait, that's illegal.")}</Heading>
        </div>
    </ErrorContainer>
)

export default Unauthorized
