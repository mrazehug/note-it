import React, {useState, useEffect, useContext} from 'react';
import LanguageContext from "~/utility/LanguageContext";
import {loadLanguage} from "~/utility/helpers";

export const ProvideLanguage = ({children}) => {
    const lang = useProvideLanguage();

    return <LanguageContext.Provider value={lang}>{children}</LanguageContext.Provider>;
}

export default function useLanguage(){
    return useContext(LanguageContext);
}

function useProvideLanguage()
{
    const [language, setLanguage] = useState()

    useEffect(()=>{
        setLanguage(loadLanguage());
    }, [])

    return {
        language, setLanguage
    };
}
