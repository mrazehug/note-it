import axios from "axios";
import camelcaseKeys from "camelcase-keys";
import snakeCaseKeys from 'snakecase-keys';
import {authConfig} from "~/api/authConfig";
import history from "~/utility/history";
import QueryString from 'query-string';
import {isProduction} from "~/utility/helpers";

let isRefreshing = false;
let failedQueue = [];


class NoteItAxios {
    baseURL;
    version;

    constructor(baseURL = isProduction() ? "https://wa.toad.cz/~mrazehug/api" : "http://localhost:8000/api", baseVersion = "v1") {
        this.baseURL = baseURL;
        this.version = baseVersion;
    }

    async post(url, data, config = {}) {
        const completeURL = this.getURL(url);
        const completeConfig = this.getConfig(config);

        return axios.post(completeURL, data, completeConfig)
            .then((response)=>camelcaseKeys(response, {deep: true}))
            .catch((error) => {
                console.log(error);
                return this.handleError(error);
            });
    }

    async get(url, options = {}, config = {}) {
        const completeURL = this.generateQueryString(this.getURL(url), options);
        const completeConfig = this.getConfig(config);

        return axios.get(completeURL, completeConfig)
            .then((response)=>camelcaseKeys(response, {deep: true}))
            .catch((error) => this.handleError(error));
    }

    async patch(url, data, config = {}) {
        const completeURL = this.getURL(url);
        const completeConfig = this.getConfig(config);

        return axios.patch(completeURL, data, completeConfig)
            .then((response)=>camelcaseKeys(response, {deep: true}))
            .catch((error) => this.handleError(error));
    }

    async delete(url, config = {}) {
        const completeURL = this.getURL(url);
        const completeConfig = this.getConfig(config);

        return axios.delete(completeURL, completeConfig)
            .then((response)=>camelcaseKeys(response, {deep: true}))
            .catch((error) => this.handleError(error));
    }

    getConfig(config) {
        return {
            ...config,
            headers: {
                ...config.headers,
                Authorization: `Bearer ${authConfig.accessToken}`,
            },
            withCredentials: true,
        };
    }

    getURL(url) {
        const safeURL = url.startsWith("/") ? url.slice(1) : url;
        return `${this.baseURL}/${this.version}/${safeURL}`;
    }

    generateQueryString(url, options) {
        const snakeOptions = snakeCaseKeys(options, {deep: true});
        return Object.keys(options).length === 0 ? url : `${url}?${QueryString.stringify(snakeOptions, { arrayFormat: "bracket", sort: false })}`;
    }

    async handleError(error) {
        if (error.response.status !== 401) { return Promise.reject(error); }
        else if(error.response.status === 401 && !error.config._retry)
        {
            if(isRefreshing)
            {
                return new Promise(function (resolve, reject){
                    failedQueue.push({resolve, reject})
                }).then((token)=>{
                    error.config.headers['Authorization'] = `Bearer ${token}`;
                    return axios(error.config);
                }).catch((err)=>{
                    return Promise.reject(err)
                })
            }

            error._retry = true;
            isRefreshing = true;
            return this.tryRefreshToken(error);
        }
        return Promise.reject(error);
    }

    async tryRefreshToken(error) {
        return axios.post(this.getURL("/login/refresh"), {}, { withCredentials: true })
            .then((response) => {
                const data = camelcaseKeys(response.data.data);
                error.response.config.headers.Authorization = `Bearer ${data.accessToken}`;
                authConfig.accessToken = data.accessToken;
                this.processFailedQueue(null, data.accessToken);
                return axios(error.response.config);
            }).catch((error) => {
                if(error.response.status === 401 && (window.location.pathname === '/profile' || window.location.pathname === '/notes'))
                {
                    history.push(`/login?next=${encodeURIComponent(window.location.pathname)}`);
                }
                this.processFailedQueue(error, null);
                return Promise.reject(error);
            })
            .finally(()=>isRefreshing = false)
    }

    processFailedQueue(error, token)
    {
        failedQueue.forEach((promise)=>{
            if(error)
                promise.reject(error);
            else
                promise.resolve(token)
        })
    }
}

export default NoteItAxios
