import React, {useState, useEffect, useContext} from 'react';
import camelcaseKeys from "camelcase-keys";
import UserService from "~/api/UserService";
import AuthService from "~/api/AuthService";
import UserContext from "~/utility/UserContext";

export const ProvideUser = ({children}) => {
    const user = useProvideUser();

    return <UserContext.Provider value={user}>{children}</UserContext.Provider>;
}

export default function useUser(){
    return useContext(UserContext);
}

function useProvideUser()
{
    const [userService] = useState(new UserService());
    const [authService] = useState(new AuthService());
    const [loggedIn, setLoggedIn] = useState(false);
    const [loading, setLoading] = useState(true);
    const [user, setUser] = useState(undefined);

    const attemptLogin = async () => {
            const loggedIn = await authService.attemptLogin();
            setLoggedIn(loggedIn);
            if(!loggedIn)
                setLoading(false);
    }

    const logout = async () => {
        authService.logout()
            .then(()=>{
                setUser(undefined);
                setLoggedIn(false);
            })
    }

    const deleteAcc = async () => {
        setUser(undefined);
        setLoggedIn(false);
    }

    useEffect(()=>{
        if(loggedIn)
        {
            userService.getUser()
                .then((response)=>{
                    const user = camelcaseKeys(response.data.data, {deep: true});
                    setUser(user);
                    setLoading(false);
                })
        }
    }, [loggedIn])

    return {
        user, loading, loggedIn, setLoggedIn, attemptLogin, logout, setUser, deleteAcc
    };
}
