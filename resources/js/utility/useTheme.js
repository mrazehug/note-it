import React, {useState, useEffect, useContext} from 'react';
import ThemeContext from "~/utility/ThemeContext";
import {loadTheme, saveTheme} from "~/utility/helpers";
import {DarkTheme, LightTheme, Themes} from "~/utility/constants";

export const ProvideTheme = ({children}) => {
    const lang = useProvideTheme();

    return <ThemeContext.Provider value={lang}>{children}</ThemeContext.Provider>;
}

export default function useTheme(){
    return useContext(ThemeContext);
}

function useProvideTheme()
{
    const [theme, setTheme] = useState({})

    useEffect(()=>{
        switch (loadTheme())
        {
            case Themes.light:
                setTheme(LightTheme);
                break;
            case Themes.dark:
                setTheme(DarkTheme);
                break;
        }
    }, [])

    const switchTheme = () => {
        switch (loadTheme())
        {
            case Themes.light:
                setTheme(DarkTheme);
                saveTheme(Themes.dark);
                break;
            case Themes.dark:
                setTheme(LightTheme);
                saveTheme(Themes.light);
                break;
        }
    }

    return {
        theme, switchTheme
    };
}
