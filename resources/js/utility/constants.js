export const Layout = {
    HEADER_HEIGHT: '50px',
    DIMENSIONS: {
        d4: '4px',
        d8: '8px',
        d16: '16px',
        d32: '32px',
        d64: '64px',
        d128: '128px'
    },
    CONTENT_TOP_PADDING: '10px'
}

export const LabelSize = {
    small: "small",
    medium: "medium",
    large: "large"
}

export const dictionary = {
    cs: {
        "Profile": "Profil",
        "Username": "Uživatelské jméno",
        "Email": "Email",
        "Password": "Heslo",
        "Save general": "Uložit obecné",
        "General": "Obecné",
        "This is how we communicate and recognize you": "Tohle je jak Vás poznáme a jak s Vámi budeme komunikovat",
        "Look and feel": "Vzhled",
        "This controls how the website looks": "Tohle určuje, jak stránka vypadá",
        "Theme": "Motiv",
        "Language": "Jazyk",
        "Loading": "Načítání",
        "This field is required": "Toto pole je povinné",
        "Labels": "Štítky",
        "Here you can customize your labels.": "Zde můžete upravovat Vaše štítky.",
        "Other actions": "Další akce",
        "Something more": "Něco víc",
        "Generate data": "Vygenerovat data",
        "Delete account": "Smazat účet",
        "How it works": "Jak to funguje",
        "Signing up, logging in": "Registrace, přihlášení",
        "To sign up, visit the register page. To log in register the login page.": "Pro registraci navštivte stránku registrace. Pro přihlášení stránku login.",
        "Your email, username and password can be changed later in the profile page.": "Váš email, uživatelské jméno a heslo mohou být později změněny na stránce profil",
        "To log out, click log out button in the bottom left corner.": "Pro odhlášení klikněte na tlačítko odhlásit se v levém dolním rohu",
        "Note management": "Správa poznámek",
        "To create a note click the square with plus on the note page.": "Pro vytvoření poznámky klikněte na čtvereček s plus na stránce poznámky.",
        "Default note is created. You can start modifying it. The changes are saved automatically.": "To vytvoří výchozí poznámku. Můžete ji začít upravovat. Změny se ukládají automaticky.",
        "To update an existing note, click the 'Tt' icon in the bottom left corner of the note preview.": "Pro upravení existující poznámky, klikněte na ikonku 'Tt' v levém dolním rohu na ukázce poznámky.",
        "To delete a note, click the trash can icon in top right corner of the note preview": "Pro smazání poznámky klikněte na ikonku koše v pravém horním rohu na ukázce poznámky.",
        "Label management": "Správa štítků",
        "To create or a label, go to profile page. Here you can manage labels. Note that": "Pro vytvoření štítku jděte na stránku profilu. Tady můžete upravovat štítky.",
        "if you delete a label, it will be removed form all notes with it.": "Pozor na to, že když štítek smažete, bude smazán i ze všech poznámek.",
        "Once label is created it can be added to the note.": "Jakmile štítek vytvoříte, může být přidán k poznámce.",
        "Other settings": "Další nastavení",
        "Profile tab can be accessed by clicking the person icon in top right corner. In profile tab theme and language can be changed.": "Do záložky profil se dostanete kliknutím na ikonku človíčka v pravé horní části obrazovky. V záložce profilu můžete měnit motiv a jazyk stránky.",
        "The definitive app for taking notes.": "Ultimátní aplikace na správu poznámek.",
        "Insert cheesy startup line here. Could also brag about who uses us. Wrote more text so the design looks as I want it to look, lol. Another compilation later it is still not long enough. There you go. Sort of.": /*This line is getting out of hand, doesn't it?*/ "Sem vložte trapnou poznámku, co používají startupy. Taky tu můžeme machrovat s tím, kdo všechno nás používá. Napsal jsem víc textu, aby to designově vypadalo dobře. Po další kompilaci jsem zjistil, "+/*Reeeally out of hand. Neighbours cubicle starts in a couple of characters.*/"že je to málo textu. Tak, to už stačí.",
        "Note anything.": "Poznamenejte cokoli.",
        "Note anywhere.": "Poznamenejte kdekoli.",
        "Note anytime.": "Poznamenejte kdykoli.",
        "Log in": "Přihlásit se",
        "Register": "Zaregistrovat se",
        "Notes": "Poznámky",
        "Working on it": "Pracujeme na tom",
        "Email or username": "Email nebo uživatelské jméno",
        "Confirm password": "Potvrdit heslo",
        "The passwords do not match": "Hesla se neshodují",
        "Min length is 8": "Minimální délka je 8",
        "The email has already been taken.": "Tento email už někdo používá.",
        "The username has already been taken.": "Toto uživatelské jméno už někdo používá",
        "Already registered?": "Už jste zaregistrovaní?",
        "Register with us to start taking notes": "Zaregistrujte se pro tvorbu poznámek",
        "Not yet registered?": "Ještě nejste registrovaní?",
        "Login to start taking notes": "Přihlaste se pro psaní poznámek",
        "Email, username or password is wrong": "Email, uživatelské jméno nebo heslo jsou špatně.",
        "The email must be a valid email address.": "Email musí být validní emailová adresa",
        "Filters": "Filtry",
        "Run filters": "Aplikuj filtry",
        "Create new label": "Vytvořit štítek",
        "Name": "Jméno",
        "Color": "Barva",
        "Save": "Uložit",
        "Could not find what you were looking for": "Nedokázali jsme najít to, co jste hledali.",
        "Wait, that's illegal.": "K tomuto jste oprávněni.",
        "The username format is invalid.": "Obsahuje nepovolené znaky.",
        "Last updated": "Naposledy upraveno",
        "No labels.": "Žádné štítky"
    },
    en: {
        "Profile": "Profile"
    }
}

export const languages = [
    { value: 'cs', label: 'Česky' },
    { value: 'en', label: 'English' },
];

export const Themes = {
    light: 'light',
    dark: 'dark'
}

export const DarkTheme = {
    bg: '#363636',
    fg: 'white',
    borderColor: '#a1a1a1',
    accent: '#8f8f8f'
}

export const LightTheme = {
    bg: 'white',
    borderColor: '#a1a1a1',
    accent: '#4b5cfa'
}
