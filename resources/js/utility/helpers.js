import keys from 'object-keys';
import {dictionary, languages, Themes} from "~/utility/constants";
import Cookie from 'js-cookie';
import useLanguage from "~/utility/useLanguage";

export const transformFormErrors = (errors) => {
    const rv = {}
    keys(errors).map(key=>rv[key] = errors[key][0]);
    return rv;
}

const extractLang = (lang) => {
    if(lang.includes('en'))
        return 'en';
    if(lang.includes('cs'))
        return 'cs';

    return 'en';
}

/**
 * Translating function.
 * @param label - Label to translate
 * @return {*} - Translated label
 */
export const t = (label) => {
    let lang = Cookie.get('lang');
    if(!lang)
    {
        lang = extractLang(window.navigator.language);
        Cookie.set('lang', lang,  { expires: 365 });
    }
    const translation = dictionary[lang][label];
    return translation ? translation : label;
}

export const setLanguage = (lang) => {
    Cookie.set('lang', lang);
}

export const getLangValue = () => {
    const lang = Cookie.get('lang');
    return languages.filter(opt => opt.value === lang)[0];
}

export const loadTheme = () => {
    const theme = Cookie.get('theme');
    if(!theme) {
        Cookie.set('theme', Themes.dark, { expires: 365 });
        return loadTheme();
    }
    return theme
}

export const loadLanguage = () => {
    const lang = Cookie.get('lang');
    if(!lang) {
        Cookie.set('lang', extractLang(window.navigator.language), { expires: 365 });
        return loadTheme();
    }
    return lang;
}

export const saveTheme = (theme) => {
    Cookie.set('theme', theme, { expires: 365 });
}

export const setTitle = (title = '') => {
    if(title !== '')
        document.title = `note-it | ${title}`;
    else
        document.title = `note-it`;
}

export const isProduction = () => {
    return process.env.NODE_ENV !== 'development';
}

export const to = (path) => {
    return isProduction() ? `/~mrazehug${path}` : path;
}

export const setFormErrors = (errors, setError) => {
    keys(errors).forEach(key=>setError(key, { type: "manual", message: errors[key][0]}));
}
