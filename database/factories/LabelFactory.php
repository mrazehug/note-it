<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Labels\Model\Label;
use Faker\Generator as Faker;

$factory->define(Label::class, function(Faker $faker){
    return [
        'name' => 'Test name',
        'color' => '#FBAFFF'
    ];
});
