<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Notes\Model\Note;
use Faker\Generator as Faker;

$factory->define(Note::class, function (Faker $faker){
    return [
        'title' => 'Test title',
        'text' => 'Test text'
    ];
});
