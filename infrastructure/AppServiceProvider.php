<?php


namespace Infrastructure;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Route::group([], base_path('routes/web.php'));
    }
}
