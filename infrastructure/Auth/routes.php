<?php

Route::post('/login', 'LoginController@login')
    ->name('login');
Route::post('/login/refresh', 'LoginController@refresh')
    ->name('refresh');
Route::post('/login/attempt', 'LoginController@attempt')
    ->middleware('auth:api')
    ->name('attempt');
Route::post('/register', 'RegisterController@register')
    ->name('register');

Route::post('/logout', 'LoginController@logout')
    ->middleware('auth:api')
    ->name('logout');
