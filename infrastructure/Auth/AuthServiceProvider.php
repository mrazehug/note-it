<?php

namespace Infrastructure\Auth;

use Carbon\Carbon;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        Passport::routes(function($router){
            $router->forAccessTokens();
            $router->forPersonalAccessTokens();
            $router->forTransientTokens();
        });

        Passport::tokensExpireIn(Carbon::now()->addMinutes(App::environment('local') ? 60 * 24 * 10 : 10));
        Passport::refreshTokensExpireIn(Carbon::now()->addDays(10));

        $this->defineRoutes();
    }

    private function defineRoutes()
    {
        Route::group([
            'namespace' => 'Infrastructure\Auth\Controllers',
            'prefix' => '/api/v1'
        ], base_path('infrastructure/Auth/routes.php'));
    }
}
