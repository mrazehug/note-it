<?php


namespace Infrastructure\Auth\Controllers;


use App\Users\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\UnauthorizedException;
use Infrastructure\Auth\Proxies\LoginProxy;
use Infrastructure\Auth\Services\FacebookTokenService;
use Infrastructure\Http\Controller;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

/**
 * @group Auth
 *
 * APIs for managing auth.
 */

class SocialController extends Controller
{
    private $loginProxy;

    public function __construct(LoginProxy $loginProxy)
    {
        $this->loginProxy = $loginProxy;
    }

    public function process(Request $request, $platform)
    {
        return $this->loginProxy->proxy('social', [
            'provider' => $platform,
            'access_token' => $request->get('accessToken')
        ]);
    }
}
