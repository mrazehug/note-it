<?php


namespace Infrastructure\Auth\Controllers;


use Infrastructure\Auth\Proxies\RegisterProxy;
use Infrastructure\Auth\Requests\RegisterRequest;
use Infrastructure\Http\Controller;

/**
 * @group Auth
 *
 * APIs for managing auth.
 */

class RegisterController extends Controller
{
    private $registerProxy;

    public function __construct(RegisterProxy $registerProxy)
    {
        $this->registerProxy = $registerProxy;
    }

    /**
     * Register
     * Registers user.
     */
    public function register(RegisterRequest $request)
    {
        $data = $this->getRequestData($request);

        return $this->response($this->registerProxy->attemptRegister($data));
    }
}
