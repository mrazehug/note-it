<?php


namespace Infrastructure\Auth\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Auth\Proxies\LoginProxy;
use Infrastructure\Auth\Requests\LoginRequest;
use Infrastructure\Http\Controller;


/**
 * @group Auth
 *
 * APIs for managing auth.
 */

class LoginController extends Controller
{
    private $loginProxy;

    public function __construct(LoginProxy $loginProxy)
    {
        $this->loginProxy = $loginProxy;
    }

    /**
     * Login
     * Logs in user with valid credentials.
     *
     * @bodyParam email string required The users email
     * @bodyParam password string required The users password
     * @bodyParam access_token string The access_token of the third party. If platform param is filled in, this field is required.
     * @queryParam platform optional The platform using which the user should be logged in. Acceptable values are: facebook
     *
     */
    public function login(LoginRequest $request)
    {
        if($request->has('platform'))
        {
            return $this->response($this->loginProxy->proxy('social', [
                'provider' => $request->get('platform'),
                'access_token' => $request->get('access_token')
            ]));
        }

        $email = $request->get('email');
        $password = $request->get('password');

        return $this->response($this->loginProxy->attemptLogin($email, $password));
    }

    /**
     * Refresh
     * Refreshes auth_token, if the request is sent with valid remember_token.
     */
    public function refresh(Request $request)
    {
        return $this->response($this->loginProxy->attemptRefresh());
    }

    /**
     * Attempt
     * Attempts to login user
     */
    public function attempt()
    {
        return $this->response(['message' => 'Success'], 200);
    }

    /**
     * Logout
     * Logs user out.
     */
    public function logout()
    {
        $this->loginProxy->logout();

        return $this->response(null, 204);
    }
}
