<?php


namespace Infrastructure\Auth\Proxies;


use App\Users\Repositories\UserRepository;

class RegisterProxy
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function attemptRegister($data)
    {
        $user = $this->userRepository->create($data);

        return $user;
    }
}
