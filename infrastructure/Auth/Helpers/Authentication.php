<?php


namespace Infrastructure\Auth\Helpers;

use Illuminate\Support\Facades\Auth;

class Authentication
{
    public function getCurrentUser()
    {
        return Auth::user();
    }

}
