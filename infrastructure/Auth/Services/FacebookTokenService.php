<?php


namespace Infrastructure\Auth\Services;


use App\Users\Repositories\UserRepository;
use GuzzleHttp\Client;
use Illuminate\Support\Str;
use Infrastructure\Auth\Proxies\LoginProxy;
use Infrastructure\Auth\Model\FacebookLogin;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class FacebookTokenService
{
    private $client;

    private $userRepository;

    public function __construct(Client $client, UserRepository $userRepository)
    {
        $this->client=$client;
        $this->userRepository = $userRepository;
    }

    public function getUserInfo($token)
    {
        $response = $this->client->request('GET', 'https://graph.facebook.com/me', [
            'query' => [
                'access_token' => $token,
                'fields' => 'email, name'
            ]
        ]);

        return (array)((array)json_decode($response->getBody()));

    }
}
