<?php


namespace Infrastructure\Auth\Middleware;


use Closure;
use Infrastructure\Auth\Helpers\Authentication;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class Heimdall
{
    private $auth;

    public function __construct(Authentication $auth)
    {
        $this->auth=$auth;
    }

    public function handle($request, Closure $next)
    {
        return $next($request);
    }
}
