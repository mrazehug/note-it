<?php


namespace Infrastructure\Auth\Requests;


use Infrastructure\Http\ApiRequest;

class RegisterRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'username' => 'required|unique:users|regex:/^[a-zA-Z.]*$/',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'confirm_password' => 'required|same:password'
        ];
    }
}
