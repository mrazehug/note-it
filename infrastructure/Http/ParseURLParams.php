<?php


namespace Infrastructure\Http;



use Illuminate\Http\Request;

trait ParseURLParams
{
    private function parseURLParams(Request $request)
    {
        $urlData = parse_url($request->fullUrl());

        if (empty($urlData['query'])) { return null; }

        $query = explode("&", $urlData['query']);
        $parameters = array();

        foreach($query as $parameter) {
            $param = explode("=", $parameter);

            if (!empty($param) && count($param) == 2) {
                if(str_contains(urldecode($param[1]), ','))
                {
                    $arr = array();
                    foreach (explode(',', urldecode($param[1])) as $item)
                    {
                        array_push($arr, urldecode($item));
                    }
                    $parameters[$param[0]] = $arr;
                }
                else
                    $parameters[$param[0]] = urldecode($param[1]);
            }
        }

        return $parameters;
    }
}
