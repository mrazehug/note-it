<?php


namespace Infrastructure\Http;


use Carbon\Laravel\ServiceProvider;
use Illuminate\Support\Facades\Broadcast;

class BroadcastServiceProvider extends ServiceProvider
{

    public function boot()
    {
        Broadcast::routes(['middleware'=>['auth:api']]);

        require base_path('packages/musonza/chat/src/Http/channels.php');
    }
}
