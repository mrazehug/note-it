<?php


namespace Infrastructure\Http\Middleware;


use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class SnakeToCamel
{
    public function handle(Request $request, Closure $next)
    {
        $request->merge($this->camelKeys($request->all()));

        return $next($request);
    }

    function camelKeys($array)
    {
        $result = [];
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $value = $this->camelKeys($value);
            }
            $result[Str::camel($key)] = $value;
        }
        return $result;
    }
}
