<?php


namespace Infrastructure\Http;


use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;
use Illuminate\Validation\UnauthorizedException;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

abstract class ApiRequest extends FormRequest
{

    protected $resourceDataKey;

    protected $ability;

    protected $isCreateRequest = false;

    public function __construct(array $query = [], array $request = [], array $attributes = [], array $cookies = [], array $files = [], array $server = [], $content = null)
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
        $this->ability = $this->getAbility();
    }

    private function getAbility()
    {
        $className = explode('\\', get_class($this));
        $className = array_pop($className);
        $ability = str_replace('Request', '', $className);
        $ability = preg_replace('/\B([A-Z])/', '-$1', $ability);
        $ability = mb_strtolower($ability);
        return $ability;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new BadRequestException($validator->errors()->toJson());
    }

    protected function failedAuthorization()
    {
        throw new UnauthorizedException('');
    }

    protected function arrayKeysToCamel(array $input)
    {
        $rv = [];
        foreach (array_keys($input) as $key)
        {
            if(is_array($input[$key]))
            {
                $this->arrayKeysToCamel($input[$key]);
            }
            else if(is_object($input[$key]))
            {
                $this->objectKeysToCamel($input[$key]);
            }
            else
            {
                array_push($rv, [Str::camel($key) => $input[$key]]);
            }
        }
        return $rv;
    }

    protected function objectKeysToCamel(object $input)
    {
        return (object)$this->arrayKeysToCamel((array)$input);
    }

    public function rules()
    {
        return [];
    }
}
