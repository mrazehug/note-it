<?php

namespace Infrastructure\Http;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Str;
use JsonSerializable;
use League\Fractal\Manager;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected function response($data, $statusCode = 200, array $headers = [])
    {
        if($statusCode == 204)
            return new JsonResponse(null, $statusCode, $headers);

        if ($data instanceof Arrayable && !$data instanceof JsonSerializable) {
            $data = $data->toArray();
        }

        return new JsonResponse(['data' => $data], $statusCode, $headers);
    }

    function camelKeys($array)
    {
        $result = [];
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $value = $this->camelKeys($value);
            }
            $result[Str::camel($key)] = $value;
        }
        return $result;
    }

    protected function getRequestData(ApiRequest $request)
    {
        return $this->camelKeys($request->validated());
    }

    protected function parseIncludes($request)
    {
        fractal()->parseIncludes($request->get('include'));
    }
}
