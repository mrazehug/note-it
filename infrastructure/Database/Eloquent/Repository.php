<?php


namespace Infrastructure\Database\Eloquent;


abstract class Repository
{
    public abstract function getModel();
}
