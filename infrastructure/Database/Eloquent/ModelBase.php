<?php


namespace Infrastructure\Database\Eloquent;


use Illuminate\Database\Eloquent\Model;

class ModelBase extends Model
{
    public $timestamps = false;

    public function getAttribute($key)
    {
        if (array_key_exists($key, $this->relations)) {
            return parent::getAttribute($key);
        } else {
            return parent::getAttribute(snake_case($key));
        }
    }

    public function setAttribute($key, $value)
    {
        return parent::setAttribute(snake_case($key), $value);
    }
}
