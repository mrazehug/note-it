<?php


namespace Infrastructure\Database\Eloquent;


interface IMessagable
{
    public function messagable();
}
