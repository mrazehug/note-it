<?php

namespace Infrastructure\Exceptions;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\UnauthorizedException;
use Infrastructure\Auth\Exceptions\InvalidCredentialsException;
use Laravel\Passport\Exceptions\MissingScopeException;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if($exception instanceof NotFoundHttpException)
        {
            return response()->json(['message'=>'Not found'])->setStatusCode(404);
        }
        else if($exception instanceof ModelNotFoundException)
        {
            return response()->json(['message'=>'Resource not found'])->setStatusCode(404);
        }
        else if($exception instanceof UnauthorizedHttpException
                || $exception instanceof UnauthorizedException)
        {
            $code = 401;

            if($exception instanceof UnauthorizedException)
                $code = 403;

            return response()->json(['message' => $exception->getMessage()])->setStatusCode($code);
        }
        else if($exception instanceof MissingScopeException || $exception instanceof ForbiddenException)
        {
            return response()->json(['message'=>'Wait, that\'s illegal'])->setStatusCode(403);
        }
        else if($exception instanceof BadRequestException)
        {
            return response()->json(['message'=>'Cannot process', 'errors'=>json_decode($exception->getMessage())])->setStatusCode(400);
        }
        else if($exception instanceof InvalidCredentialsException)
        {
            return response()->json(['message' => 'Unauthorized'])->setStatusCode(401);
        }
        else if($exception instanceof HttpException)
        {
            if($exception->getStatusCode() == 403)
                return response()->json(['message'=>'Wait, that\'s illegal'])->setStatusCode(403);
        }

        if (App::environment('local') || App::environment('testing'))
        {
            return parent::render($request, $exception);
        }
        else
        {
            return response()->json(['message'=>'Something went wrong on our end.', 'msg'=>$exception->getMessage()])->setStatusCode(500);
        }
    }
}
