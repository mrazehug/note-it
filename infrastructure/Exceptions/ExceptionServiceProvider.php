<?php


namespace Infrastructure\Exceptions;


use Carbon\Laravel\ServiceProvider;
use Illuminate\Contracts\Debug\ExceptionHandler;

class ExceptionServiceProvider extends ServiceProvider
{
    public function boot()
    {
        /**
         * Do not forget to import them before using!
         */
        $this->app->bind(
            ExceptionHandler::class,
            Handler::class
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
